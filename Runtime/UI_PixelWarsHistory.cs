﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PixelWarsHistory : MonoBehaviour
{

    public Text[] m_lastCommands;


    public void Push(string message) {
        if (m_lastCommands.Length <=2)
            return;
        for (int i = m_lastCommands.Length-1; i >0; i--)
        {
            m_lastCommands[i].text = m_lastCommands[i - 1].text;
        }
        m_lastCommands[0].text = message;
    }
    public void Push(UnityPlayerMetaInformation player, string message) {

        Push(player.m_playerId.GetNameWithoutPlatform()+": "+message);
    }
}
