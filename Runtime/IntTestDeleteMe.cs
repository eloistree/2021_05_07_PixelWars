﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntTestDeleteMe : MonoBehaviour
{

    public string m_stringIntId;
    public int m_intId;
    public double m_r;
    public double m_g;
    public double m_b;
    public uint m_ri;
    public uint m_gi;
    public uint m_bi;

    private void OnValidate()
    {
            int.TryParse(m_stringIntId,out  m_intId);

        m_r = (m_intId) * 0.000001f;
        m_g = (m_intId * 0.001f);
        m_b = (m_intId); 

        m_ri = (uint)(((double)m_intId) * 0.000001f ) ;
        m_gi = (uint)(((double)m_intId) * 0.001f - (1000 * (int)m_r) );
        m_bi = (uint)(((double)m_intId) - (1000 * (int)m_g) ) ;
    }
}
