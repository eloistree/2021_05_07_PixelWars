﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWorldFocusTransition : MonoBehaviour
{
    public CameraWorldFocus m_affected;
    [Range(-180f,180f)]
    public float m_wantedLatitude;
    [Range(-90f, 90f)]
    public float m_wantedLongitude;
    [Range(0f,1f)]
    public float m_wantedFocusInPourcent=1f;
    public float m_translationSpeed=45;

    void Update()
    {
        if (m_wantedLatitude >= 180f)
        {
            m_wantedLatitude = -360 + m_wantedLatitude;
            m_affected.SetLatitude ( m_wantedLatitude);
            m_affected.RefreshTransformPosition();
        }
        else if (m_wantedLatitude <= -180f)
        {
            m_wantedLatitude = 360 + m_wantedLatitude;
            m_affected.SetLatitude ( m_wantedLatitude);
            m_affected.RefreshTransformPosition();
        }
        //else if (m_wantedLongitude > 90f)
        //{
        //    m_wantedLongitude = -180 + m_wantedLongitude;
        //    m_affected.SetLongitude( m_wantedLongitude);
        //    m_affected.RefreshTransformPosition();
        //}
        //else if (m_wantedLongitude < -90)
        //{
        //    m_wantedLongitude = 180 + m_wantedLongitude;
        //    m_affected.SetLongitude(  m_wantedLongitude);
        //    m_affected.RefreshTransformPosition();
        //}
        else { 
            m_affected.SetLongitude ( Mathf.Lerp(m_affected.m_longitude, m_wantedLongitude, Time.deltaTime*m_translationSpeed));
            m_affected.SetLatitude( Mathf.Lerp(m_affected.m_latitude, m_wantedLatitude, Time.deltaTime * m_translationSpeed));
            m_affected.SetFocusAsPourcent(  Mathf.Lerp(m_affected.m_cameraFocusInPourcent, m_wantedFocusInPourcent, Time.deltaTime * m_translationSpeed));
            m_affected.RefreshTransformPosition();
        }
    }

    public void OverrideLatitude(float latitude) {
        m_affected.m_latitude = m_wantedLatitude = latitude;
    }
}


