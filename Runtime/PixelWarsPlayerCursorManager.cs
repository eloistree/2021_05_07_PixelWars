﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelWarsPlayerCursorManager : MonoBehaviour
{
    public List<UnityPlayerCursorInformation > m_cursorInGame;

    public void TryMoveCursor(PlayerNameId playerName, float l2r, float b2t)
    {
        HasCursor(playerName, out bool found, out UnityPlayerCursorInformation player);

        if (found)
        {
            player.SetPosition(l2r, b2t);
        }
    }
    public void TryMoveCursorOrCreate(PlayerNameId playerName, float l2r, float b2t)
    {
        HasCursor(playerName, out bool found, out UnityPlayerCursorInformation player);

        if (found)
        {
            player.SetPosition(l2r, b2t);
        }
        else { CreateCursorFor(l2r, b2t, playerName); }
    }

    public void CreateCursorFor(PlayerNameId playerId)
    {
        CreateCursorFor(0f, 0f, playerId);
    }

    public void CreateCursorFor(float l2r, float b2t, PlayerNameId playerId)
    {
        HasCursor(playerId, out bool found, out UnityPlayerCursorInformation player);
        if (found) return;

        UnityPlayerCursorInformation newCursor = new UnityPlayerCursorInformation(playerId);
        m_cursorInGame.Add(newCursor);
        newCursor.SetPosition(l2r, b2t);
    }

    public void HasCursor(PlayerNameId playerName,out  bool found , out UnityPlayerCursorInformation player) {
        for (int i = 0; i < m_cursorInGame.Count; i++) { 

            if(  m_cursorInGame[i].m_playerId == playerName)
            {
                found = true;
                player = m_cursorInGame[i];
                return;
            }
        }

        found = false;
        player = null;
    }    
}

[System.Serializable]
public class UnityPlayerCursorInformation {

    public static DateTime m_2020 = new DateTime(2020, 01, 01);
    public PlayerNameId m_playerId;
    public TeamColor m_cursorColor;
    public PlayerCursorForShader m_cursorMirror = new PlayerCursorForShader();
    public DateTime m_changeTime = DateTime.Now;

    private void SetDate(DateTime now) { m_changeTime = now; }
    private void SetDate() { m_changeTime = DateTime.Now; }
    public double GetMilliSecondPast()
    {
        return GetMilliSecondPast(DateTime.Now);
    }
    public double GetMilliSecondPast(DateTime now)
    {
        return (now - m_changeTime).TotalMilliseconds;
    }
    public double GetSecondPast()
    {
        return GetSecondPast(DateTime.Now);
    }
    public double GetSecondPast(DateTime now)
    {
        return (now - m_changeTime).Seconds;
    }
    public UnityPlayerCursorInformation(PlayerNameId playerId)
    {
        m_playerId = playerId;
        m_cursorMirror.SetPlayerId(playerId.m_inGameId);
    }
    public void SetPosition(float l2rPourcent, float b2tPourcent) {
        m_cursorMirror.SetPosition(l2rPourcent, b2tPourcent);
        SetDate();
    }

    public float GetBot2TopPct()
    {
        return m_cursorMirror.m_bot2TopPourcent;
    }

    public float GetLeft2RightPct()
    {
        return m_cursorMirror.m_left2RightPourcent;
    }

    public void SetCurrentColor(Color color) { m_cursorColor = new  TeamColor (color); SetDate(); }
    public void SetCurrentColor(TeamColor color) { m_cursorColor = (color); SetDate(); }
    public TeamColor GetCurrentTeamTarget() { return m_cursorColor; }

    public void GetPosition(out float l2rPct, out float b2tPct)
    {
        l2rPct = m_cursorMirror.m_left2RightPourcent;
        b2tPct = m_cursorMirror.m_bot2TopPourcent; 
    }
    public void GetColor(out Color color)
    {
        color = m_cursorColor.m_teamColor;
    }

    public PourcentToPixelCoordinate GetPctPixel()
    {
        return new PourcentToPixelCoordinate(m_cursorMirror.m_left2RightPourcent, m_cursorMirror.m_bot2TopPourcent);
    }

}

[System.Serializable]
public struct PlayerCursorForShader {

    public int m_playerId;
    public float m_left2RightPourcent;
    public float m_bot2TopPourcent;

    public void SetPosition(float l2rPourcent, float b2tPourcent)
    {
        m_left2RightPourcent = l2rPourcent;
        m_bot2TopPourcent = b2tPourcent;
    }

    public void SetPlayerId(int inGameId)
    {
        m_playerId = inGameId;
    }
}