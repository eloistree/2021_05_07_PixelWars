﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ComputeShaderGetterPixelWars : MonoBehaviour
{
    public PixelWarsStateMono m_pixelWarsState;
    public PixelWarsPlayerCursorManager m_cursorManager;
    private bool m_hasBeenInit;


    [Header("A")]
    public ComputeShader m_shaderValidePixel;
    public ComputeBuffer m_validateComputeBuffer;
    public RenderTexture m_validateMapBUffer;
    public RawImage[] m_validateMapImage;
    public Material[] m_validateMapImageMaterial;
    [Header("A")]
    public ComputeShader m_shaderToRendering;
    public ComputeBuffer m_claimedComputeBuffer;
    public RenderTexture m_claimedMapBUffer;
    public RawImage[] m_claimedMapImage;
    public Material[] m_claimedMapImageMaterial;

    [Header("A")]
    public ComputeBuffer m_populationComputeBuffer;
    public RenderTexture m_populationMapBuffer;
    public RawImage[] m_populationMapImage;

    [Header("A")]
    public ComputeBuffer m_populationStaticComputeBuffer;
    public RenderTexture m_populationStaticMapBuffer;
    public RawImage[]  m_populationStaticMapImage;


    [Header("A")]

    public ComputeShader     m_pixelCountPerTeam;
    public ComputeBuffer     m_teamObservedBuffer;
    public ComputeBuffer m_pixelFoundBuffer;
    public ComputeBuffer m_troupFoundBuffer;
    public ComputeBuffer m_troupStaticFoundBuffer;
    public TeamsStateInMap m_teamsState;
    public TeamsStateInMap m_teamsStateForUI;



    [Header("A")]
    public ComputeShader m_cursorDisplayer;
    public int [] m_cursorColor;
    public float [] m_cursorLastChange;
    public ComputeBuffer m_cursorsComputeBuffer;
    public ComputeBuffer m_cursorsTimeComputeBuffer;
    public RenderTexture m_textureCursorsMapBuffer;
    public RawImage[] m_cursorsMapImage;
    public Material[] m_cursorMapImageMaterial;
    public float m_cursorRadius = 0.01f;
    public int m_maxDisplayTimeSecond = 5000;


    public double m_renderingT;
    public double m_teamStateT;
    public double m_cursorT;




    void Start()
    {

        InvokeRepeating("RefreshUI", 0.1f, 0.1f);
        
    }
    public RenderTextureFormat m_textureType;
    public void RefreshUI()
    {
     
     


        if (m_pixelWarsState == null ||
            m_pixelWarsState.m_mapState == null ||
            m_pixelWarsState.m_mapState.GetPixelCount() <= 0)
            return;
        int kernel;
        uint count = m_pixelWarsState.m_mapState.GetPixelCount();
        int width = (int)m_pixelWarsState.m_mapState.GetPixelWidth();
        int height = (int)m_pixelWarsState.m_mapState.GetPixelHeight();
        int teamObservedCount = m_teamsState.m_maxTeamObserved;
        if (!m_hasBeenInit)
        {
            m_hasBeenInit = true;
            m_teamsState.m_teamsObserved = new int[teamObservedCount];
            m_teamsState.m_pixeldFoundPerTeam = new int[teamObservedCount];
            m_teamsState.m_troupFoundPerTeam = new int[teamObservedCount];
            m_teamsState.m_troupStaticFoundPerTeam = new int[teamObservedCount];
            m_teamsState.m_validePixelCount = (int)m_pixelWarsState.GetValidePixelCount();
            m_teamsStateForUI.m_validePixelCount = (int)m_pixelWarsState.GetValidePixelCount();

            m_teamObservedBuffer = new ComputeBuffer(teamObservedCount, sizeof(int), ComputeBufferType.Default);
            m_pixelFoundBuffer = new ComputeBuffer(teamObservedCount, sizeof(int), ComputeBufferType.Default);
            m_troupFoundBuffer = new ComputeBuffer(teamObservedCount, sizeof(int), ComputeBufferType.Default);
            m_troupStaticFoundBuffer = new ComputeBuffer(teamObservedCount, sizeof(int), ComputeBufferType.Default);


            //COMPUTE BUFFER
            m_claimedComputeBuffer = new ComputeBuffer((int)count, sizeof(int), ComputeBufferType.Default);
            m_populationComputeBuffer = new ComputeBuffer((int)count, sizeof(int), ComputeBufferType.Default);
            m_populationStaticComputeBuffer = new ComputeBuffer((int)count, sizeof(int), ComputeBufferType.Default);
            m_validateComputeBuffer = new ComputeBuffer((int)count, sizeof(int), ComputeBufferType.Default);

            //SET TEXTURE AND RAW IMAGE

            m_validateMapBUffer = new RenderTexture((int)width, (int)height, 1, m_textureType);
            m_validateMapBUffer.enableRandomWrite = true;
            m_validateMapBUffer.Create();

            m_claimedMapBUffer = new RenderTexture((int)width, (int)height, 1, m_textureType);
            m_claimedMapBUffer.enableRandomWrite = true;
            m_claimedMapBUffer.Create();

            m_populationMapBuffer = new RenderTexture((int)width, (int)height, 1, m_textureType);
            m_populationMapBuffer.enableRandomWrite = true;
            m_populationMapBuffer.Create();

            m_populationStaticMapBuffer = new RenderTexture((int)width, (int)height, 1, m_textureType);
            m_populationStaticMapBuffer.enableRandomWrite = true;
            m_populationStaticMapBuffer.Create();

            m_textureCursorsMapBuffer = new RenderTexture((int)width, (int)height, 1, m_textureType);
            m_textureCursorsMapBuffer.enableRandomWrite = true;
            m_textureCursorsMapBuffer.Create();

            m_cursorsComputeBuffer = new ComputeBuffer((int)count, sizeof(int), ComputeBufferType.Default);
            m_cursorsTimeComputeBuffer = new ComputeBuffer((int)count, sizeof(float), ComputeBufferType.Default);
            m_cursorColor = new int[count];
            m_cursorLastChange = new float[count];




            //SET VALIDATE COMPUTE SHADER AND PRODUCE
            kernel = m_shaderValidePixel.FindKernel("CSMain");
            m_validateComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetValidePixelAsInterger());
            Debug.Log("Humm " + m_pixelWarsState.m_mapState.GetValidePixelAsInterger().Where(k => k == 1).ToArray().Count());
            m_shaderValidePixel.SetInt("m_width", width);
            m_shaderValidePixel.SetBuffer(kernel, "m_valideTerritory", m_validateComputeBuffer);
            m_shaderValidePixel.SetTexture(kernel, "m_valideMap", m_validateMapBUffer);
            m_shaderValidePixel.Dispatch(kernel, width / 8, height / 8, 1);


            foreach (var t in m_validateMapImageMaterial)
            {
                t.mainTexture = m_validateMapBUffer;
            }
            foreach (var t in m_claimedMapImageMaterial)
            {
                t.mainTexture = m_claimedMapBUffer;
            }
            foreach (var t in m_cursorMapImageMaterial)
            {
                t.mainTexture = m_textureCursorsMapBuffer;
            }



            foreach (var t in m_validateMapImage)
            {
                t.texture = m_validateMapBUffer;
            }
            foreach (var t in m_claimedMapImage)
            {
                t.texture = m_claimedMapBUffer;
            }
            foreach (var t in m_populationMapImage)
            {
                t.texture = m_populationMapBuffer;
            }
            foreach (var t in m_populationStaticMapImage)
            {
                t.texture = m_populationStaticMapBuffer;
            }
            foreach (var t in m_cursorsMapImage)
            {
                t.texture = m_textureCursorsMapBuffer;
            }

            m_cursorColor = new int[count];
            m_cursorLastChange = new float[count];

        }

        DateTime startChrono = DateTime.Now;
        DateTime stopChrono = DateTime.Now;





        startChrono = DateTime.Now;
        m_claimedComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetColorClaimed());
        m_populationComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetMovingTroup());
        m_populationStaticComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetStaticTroup());
        stopChrono = DateTime.Now;
        GetTime(ref startChrono, ref stopChrono, out m_setBufferT);

        startChrono = DateTime.Now;
        #region Cursor


        uint w = (uint)width;
        uint h = (uint)height;
        int radius = (int)(m_cursorRadius * (float)width);
        int radiusInner = radius - 2;
        for (int i = 0; i < m_cursorManager.m_cursorInGame.Count; i++)
        {

            float s = (float) m_cursorManager.m_cursorInGame[i].GetSecondPast();
            if (s < m_maxDisplayTimeSecond) { 
                m_cursorManager.m_cursorInGame[i].GetPosition(out float l2rPct, out float b2tPct);
                PourcentToPixelCoordinate center = new PourcentToPixelCoordinate(l2rPct, b2tPct);
                center.ApplyCircleOnPixels(ref w, ref h, ref radius, (x, y) =>
                {
                    m_cursorLastChange[x + y * width] = s;
                    m_cursorColor[x + y * width] = (int) TeamColor.GetIdFrom(Color.white);
                });
                center.ApplyCircleOnPixels(ref w, ref h, ref radiusInner, (x, y) =>
                {

                    m_cursorColor[x + y * width] = (int)m_cursorManager.m_cursorInGame[i].m_cursorColor.GetColorId();
                });
            }

        }


        m_cursorsComputeBuffer.SetData(m_cursorColor);
        m_cursorsTimeComputeBuffer.SetData(m_cursorLastChange);


        kernel = m_cursorDisplayer.FindKernel("CSMain");
        m_cursorDisplayer.SetInt("m_width", width);
        m_cursorDisplayer.SetFloat("m_maxDisplayTimeSec", m_maxDisplayTimeSecond);
        m_cursorDisplayer.SetBuffer(kernel, "m_cursors", m_cursorsComputeBuffer);
        m_cursorDisplayer.SetBuffer(kernel, "m_cursorsTimeSecond", m_cursorsTimeComputeBuffer);
        m_cursorDisplayer.SetTexture(kernel, "m_result", m_textureCursorsMapBuffer);
        m_cursorDisplayer.Dispatch(kernel, width / 8, height / 8, 1);

        m_cursorsComputeBuffer.GetData(m_cursorColor);
        m_cursorsTimeComputeBuffer.GetData(m_cursorLastChange);

        #endregion
        stopChrono = DateTime.Now;
        GetTime(ref startChrono, ref stopChrono, out m_cursorT);


        startChrono = DateTime.Now;
        #region RENDERING

        //SET VALIDATE COMPUTE SHADER AND PRODUCE
        kernel = m_shaderToRendering.FindKernel("CSMain");
        m_shaderToRendering.SetInt("m_width", width);
        m_shaderToRendering.SetInt("m_soldierMaxTransparent", (int)m_soldierMaxTransaprent);
        m_shaderToRendering.SetBuffer(kernel, "m_soldierMoveOnMap", m_populationComputeBuffer);
        m_shaderToRendering.SetBuffer(kernel, "m_soldierStaticOnMap", m_populationStaticComputeBuffer);
        m_shaderToRendering.SetBuffer(kernel, "m_colorTeamOnMap", m_claimedComputeBuffer);
        m_shaderToRendering.SetBuffer(kernel, "m_valideTerritory", m_validateComputeBuffer);


        m_shaderToRendering.SetTexture(kernel, "m_tropMoveMap", m_populationMapBuffer);
        m_shaderToRendering.SetTexture(kernel, "m_tropStaticMap", m_populationStaticMapBuffer);
        m_shaderToRendering.SetTexture(kernel, "m_claimedMap", m_claimedMapBUffer);

        m_shaderToRendering.Dispatch(kernel, width / 8, height / 8, 1);
        #endregion
        stopChrono = DateTime.Now;
        GetTime(ref startChrono, ref stopChrono, out m_renderingT);


        startChrono = DateTime.Now;
        #region TEAMSTATE

        for (int i = 0; i < m_teamsState.m_maxTeamObserved; i++)
        {
            m_teamsState.m_teamsObserved[i] = 0;
            m_teamsState.m_pixeldFoundPerTeam[i] = 0;
            m_teamsState.m_troupFoundPerTeam[i] = 0;
            m_teamsState.m_troupStaticFoundPerTeam[i] = 0;
        }
        m_teamObservedBuffer.SetData(m_teamsState.m_teamsObserved);
        m_pixelFoundBuffer.SetData(m_teamsState.m_pixeldFoundPerTeam);
        m_troupFoundBuffer.SetData(m_teamsState.m_troupFoundPerTeam);
        m_troupStaticFoundBuffer.SetData(m_teamsState.m_troupStaticFoundPerTeam);

        kernel = m_pixelCountPerTeam.FindKernel("CSMain");
        m_pixelCountPerTeam.SetInt("m_width", width);
        m_pixelCountPerTeam.SetInt("m_teamCount", m_teamsState.m_maxTeamObserved);
        m_pixelCountPerTeam.SetBuffer(kernel, "m_teams", m_teamObservedBuffer);
        m_pixelCountPerTeam.SetBuffer(kernel, "m_pixelPerTeamCount", m_pixelFoundBuffer);
        m_pixelCountPerTeam.SetBuffer(kernel, "m_soldierMoveOnMap", m_populationComputeBuffer);
        m_pixelCountPerTeam.SetBuffer(kernel, "m_soldierStaticOnMap", m_populationStaticComputeBuffer);
        m_pixelCountPerTeam.SetBuffer(kernel, "m_troupPerTeamCount", m_troupFoundBuffer);
        m_pixelCountPerTeam.SetBuffer(kernel, "m_troupStaticPerTeamCount", m_troupStaticFoundBuffer);
        m_pixelCountPerTeam.SetBuffer(kernel, "m_colorTeamOnMap", m_claimedComputeBuffer);
        m_pixelCountPerTeam.SetBuffer(kernel, "m_valideTerritory", m_validateComputeBuffer);
        m_pixelCountPerTeam.Dispatch(kernel, width / 8, height / 8, 1);


        m_pixelFoundBuffer.GetData(m_teamsState.m_pixeldFoundPerTeam);
        m_troupFoundBuffer.GetData(m_teamsState.m_troupFoundPerTeam);
        m_troupStaticFoundBuffer.GetData(m_teamsState.m_troupStaticFoundPerTeam);
        m_teamObservedBuffer.GetData(m_teamsState.m_teamsObserved);

        m_teamsState.ClampValue();
        m_teamsStateForUI.m_pixeldFoundPerTeam = m_teamsState.m_pixeldFoundPerTeam.ToArray();
        m_teamsStateForUI.m_troupFoundPerTeam = m_teamsState.m_troupFoundPerTeam.ToArray();
        m_teamsStateForUI.m_troupStaticFoundPerTeam = m_teamsState.m_troupStaticFoundPerTeam.ToArray();
        m_teamsStateForUI.m_teamsObserved = m_teamsState.m_teamsObserved.ToArray();
        #endregion
        stopChrono = DateTime.Now;
        GetTime(ref startChrono, ref stopChrono, out m_teamStateT);

    }
    public double m_setBufferT;

    private void GetTime(ref DateTime startChrono, ref DateTime stopChrono, out double valideTime)
    {
        valideTime=(stopChrono-startChrono  ).TotalSeconds;
    }

    public uint m_soldierMaxTransaprent = 100;

    private void OnDestroy()
    {
        if (m_claimedComputeBuffer != null)
            m_claimedComputeBuffer.Release();
        if (m_populationComputeBuffer != null)
            m_populationComputeBuffer.Release();
        if (m_validateComputeBuffer != null)
            m_validateComputeBuffer.Release();
        if (m_teamObservedBuffer != null)
            m_teamObservedBuffer.Release();
        if (m_pixelFoundBuffer != null)
            m_pixelFoundBuffer.Release();
    }
}
[System.Serializable]
public class TeamsStateInMap
{

    public int m_validePixelCount = int.MaxValue;
    public int m_maxTeamObserved = 1000;
    public int[] m_teamsObserved;
    public int[] m_pixeldFoundPerTeam;
    public int[] m_troupFoundPerTeam;
    public int[] m_troupStaticFoundPerTeam;
    public List<TeamInfo> m_teams = new List<TeamInfo>();
    public void BuildReadableData()
    {

        m_teams.Clear();
        for (int i = 0; i < m_teamsObserved.Length; i++)
        {
            if (m_teamsObserved[i] > 0) {
                m_teams.Add(new TeamInfo(TeamColor.GetFromUint((uint)m_teamsObserved[i]),
                m_pixeldFoundPerTeam[i],
                m_troupFoundPerTeam[i],
                m_troupStaticFoundPerTeam[i],
                m_pixeldFoundPerTeam[i] / (float) m_validePixelCount

                    )); 
            }

        }
    }

    internal void ClampValue()
    {
        for (int i = 0; i < m_teamsObserved.Length; i++)
        {
            if (m_troupFoundPerTeam[i] < 0) { m_troupFoundPerTeam[i] = 2000000000; }
            if (m_troupFoundPerTeam[i] > 2000000000) { m_troupFoundPerTeam[i] = 2000000000; }
            if (m_troupStaticFoundPerTeam[i] < 0) { m_troupStaticFoundPerTeam[i] = 2000000000; }
            if (m_troupStaticFoundPerTeam[i] > 2000000000) { m_troupStaticFoundPerTeam[i] = 2000000000; }

                
        }
    }

    internal TeamInfo [] GetBestBasedOnClaimed()
    {
        return m_teams.OrderByDescending(k => k.m_ownPixels).ToArray();
    }

    [System.Serializable]
    public class TeamInfo
    {
        public TeamColor m_team;
        public int m_ownPixels;
        public int m_troup;
        public int m_defense;
        public double m_pourcentOwned;

        public TeamInfo(TeamColor team, int ownPixels, int troup, int defense, double pourcentOwned)
        {
            m_team = team;
            m_ownPixels = ownPixels;
            m_troup = troup;
            m_defense = defense;
            m_pourcentOwned = pourcentOwned;
        }
    }

}