﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMapLinkedPosition : I_LatitudeAndLogitudePosition, I_PourcentPosition, I_IndexPosition, I_UniDimentionPosition, I_UnityTexturePositioningXY
{
    IMapDimension GetLinkedMapDimensionRef();

}


public interface I_PourcentPosition
{

    void SetL2R(double left2RightPourcent);
    void SetB2T(double bot2TopPourcent);
    void SetR2L(double left2RightPourcent);
    void SetT2B(double bot2TopPourcent);

    void GetL2R(out float left2RightPourcent);
    void GetR2L(out float right2LeftPourcent);
    void GetB2T(out float bot2TopPourcent);
    void GetT2B(out float top2BotPourcent);

    void GetL2R(out double left2RightPourcent);
    void GetR2L(out double right2LeftPourcent);
    void GetB2T(out double bot2TopPourcent);
    void GetT2B(out double top2BotPourcent);

}
public interface I_IndexPosition
{

    void SetL2R(uint left2RightPx);
    void SetB2T(uint bot2TopPx);
    void SetL2R(int left2RightPx);
    void SetB2T(int bot2TopPx);

    void GetL2R(out uint left2RightPx);
    void GetR2L(out uint right2LeftPx);
    void GetB2T(out uint bot2TopPx);
    void GetT2B(out uint top2BotPx);

    void GetL2R(out int left2RightPx);
    void GetR2L(out int right2LeftPx);
    void GetB2T(out int bot2TopPx);
    void GetT2B(out int top2BotPx);

}

public interface I_LatitudeAndLogitudePosition
{
    void SetLatitude(out double latitude);
    void SetLongitude(out double longitude);


    void GetLatitude(out double latitude);
    void GetLongitude(out double longitude);
    void GetLatitudeAndLongitude(out double latitude, out double longitude);
    void GetLatitude(out float latitude);
    void GetLongitude(out float longitude);
    void GetLatitudeAndLongitude(out float latitude, out float longitude);
}


public interface I_UnityTexturePositioningXY
{

    void Set2DTextureXY(out uint left2Right, out uint top2down);
    void Get2DTextureXY(out uint left2Right, out uint top2down);
    void Set2DTextureXY(out int left2Right, out int top2down);
    void Get2DTextureXY(out int left2Right, out int top2down);

}

public interface I_UniDimentionPosition
{
    void GetIndex(out uint index);
    void GetIndex(out int index);
    void GetLenght(out uint index);
    void GetLenght(out int index);
}



