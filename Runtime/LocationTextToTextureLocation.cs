﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LocationTextToTextureLocation : MonoBehaviour
{
    public TextAsset [] m_locationAsCsv;
    public List<TextToL2RB2T> m_imported= new List<TextToL2RB2T>();

    [System.Serializable]
    public struct TextToL2RB2T{
        public string m_name;
        public float m_l2r;
        public float m_b2t;

        public TextToL2RB2T(string name, float l2r, float b2t)
        {
            m_name = name;
            m_l2r = l2r;
            m_b2t = b2t;
        }
    }
    void Awake()
    {
        for (int i = 0; i < m_locationAsCsv.Length; i++)
        {
            string t = m_locationAsCsv[i].text;
            string[] lines = t.Split('\n');
            for (int j = 0; j < lines.Length; j++)
            {
                string[] tokens = lines[j].Split(';');
                if (tokens.Length == 4) {

                    AddTextAsPosition(tokens[0], tokens[1], tokens[2], tokens[3]);
                
                }

            }

        }
        
    }

    private void AddTextAsPosition(string name, string type, string v1, string v2)
    {
        if (type.ToLower().Trim() == "lrbt")
        {

            float.TryParse(v1, out float l2rV);
            float.TryParse(v2, out float b2tV);
            m_imported.Add(new TextToL2RB2T(name, l2rV, b2tV));
        }
        if (type.ToLower().Trim() == "map")
        {
            float.TryParse(v1, out float latitude);
            float.TryParse(v2, out float longitude);
            m_imported.Add(new TextToL2RB2T(name, ((longitude/180f)+1f)/2f, ((latitude / 90f) + 1f) / 2f));
        }
    }


    public void TryToFound(string toFind, out bool found, out float l2r, out float b2t)
    {
        found = false;
        l2r = b2t = 0;
        toFind = toFind.ToLower().Trim();
        for (int i = 0; i < m_imported.Count; i++)
        {
            if (m_imported[i].m_name.ToLower().Trim().IndexOf(toFind) > -1) {
                l2r = m_imported[i].m_l2r;
                b2t = m_imported[i].m_b2t;
                found = true;break;

            }
        }
    }
}
