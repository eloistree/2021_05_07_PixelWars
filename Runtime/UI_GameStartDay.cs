﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GameStartDay : MonoBehaviour
{
    public Text m_currentVersion;
    // Start is called before the first frame update
    void Start()
    {
        if (m_currentVersion) 
        m_currentVersion.text = string.Format("V {0:00}/{1:00}/{2:00}", DateTime.Now.Year - 2000, DateTime.Now.Month, DateTime.Now.Day);
    }

}
