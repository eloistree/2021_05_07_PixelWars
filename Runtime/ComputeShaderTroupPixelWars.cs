﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;


public class ComputeShaderTroupPixelWars : MonoBehaviour
{
    public PixelWarsStateMono m_pixelWarsState;

    [Header("A")]

    public ComputeShader m_shaderApplyAirdropTroup;
    public ComputeShader m_shaderExtractTroup;
    public ComputeShader m_shaderNaturalMove;


    [HideInInspector]
    public int[] m_extractedTroupOfMap;


    
    public List<AirDropForBuffer> m_airDrops = new List<AirDropForBuffer>();
    public AirDropForBuffer[] m_aidDropToProcess = new AirDropForBuffer[1024];


    [StructLayout(LayoutKind.Sequential)]
    public struct AirDropForBuffer{
        public int m_pixelIndex;
        public int m_troupCount;
        //0 = move , static = 1
        public int m_troupType;
        public int m_teamColorId;
        public int m_playerId;

        public AirDropForBuffer(int pixelIndex, TroupType troupType, int troupCount, int colorId, int playerNameId) : this()
        {
            m_pixelIndex = pixelIndex;
            this.m_troupType =  (troupType == TroupType.Moving ? 0 : 1);
            this.m_troupCount = troupCount;
            this.m_teamColorId = colorId;
            this.m_playerId = playerNameId;
        }
    }


    private bool m_hasBeenInit;
    public ComputeBuffer m_extractedTroupOfMapBuffer;
    public ComputeBuffer m_airDropsBuffer;



    public ComputeBuffer m_validateComputeBuffer;
    public ComputeBuffer m_claimedComputeBuffer;
    public ComputeBuffer m_populationComputeBuffer;
    public ComputeBuffer m_populationStaticComputeBuffer;
    public ComputeBuffer m_randomnessBuffer;
    public ComputeBuffer m_randomnessDirectionBuffer;
    private float[] m_randomness = new float[32];
    private uint[] m_randomdirection = new uint[32];

    public int m_width;
    public int m_height;
    public int m_pixelCount;

    public int m_enqueueCount;
    public AirDropForBuffer m_previousAdd;
    public void AddAirDrop(float left2Right, float bot2top, uint troupCount, TroupType moving, Color team, uint playerNameId)
    {
        if (!m_hasBeenInit)
            return;

        int x = (int)((left2Right) * (float)m_width);
        int y = (int)((bot2top) * (float)m_height);
        m_previousAdd = new AirDropForBuffer(x+ y*m_width, moving,(int) troupCount, (int)TeamColor.GetIdFrom(team), (int)playerNameId);
        m_airDrops.Add(m_previousAdd);
        m_enqueueCount = m_airDrops.Count;
    }

    private int[] m_troups;
    int[] m_movingTroup;
    int[] m_staticTroup;
    int[] m_claimedTeamId;
    public void DoTheThing()
    {

        if (m_pixelWarsState == null ||
            m_pixelWarsState.m_mapState == null ||
            m_pixelWarsState.m_mapState.GetPixelCount() <= 0)
            return;
        int kernel; 
        m_pixelCount = (int) m_pixelWarsState.m_mapState.GetPixelCount();
        m_width = (int) m_pixelWarsState.m_mapState.GetPixelWidth();
        m_height = (int) m_pixelWarsState.m_mapState.GetPixelHeight();
        if (!m_hasBeenInit)
        {
            m_hasBeenInit = true;

            m_claimedComputeBuffer = new ComputeBuffer((int)m_pixelCount, sizeof(int), ComputeBufferType.Default);
            m_populationComputeBuffer = new ComputeBuffer((int)m_pixelCount, sizeof(int), ComputeBufferType.Default);
            m_populationStaticComputeBuffer = new ComputeBuffer((int)m_pixelCount, sizeof(int), ComputeBufferType.Default);
            m_validateComputeBuffer = new ComputeBuffer((int)m_pixelCount, sizeof(int), ComputeBufferType.Default);
            m_extractedTroupOfMapBuffer = new ComputeBuffer((int)m_pixelCount, sizeof(int), ComputeBufferType.Default);
            m_randomnessBuffer = new ComputeBuffer(32, sizeof(float));
            m_randomnessDirectionBuffer = new ComputeBuffer(32, sizeof(uint));
            int stride =5*sizeof(int);
            m_airDropsBuffer = new ComputeBuffer(32*32, stride, ComputeBufferType.Default);
            m_troups = new int[m_pixelCount];
            m_movingTroup = new int[m_pixelCount];
            m_staticTroup = new int[m_pixelCount];
            m_claimedTeamId = new int[m_pixelCount];
        }



        m_extractedTroupOfMapBuffer.SetData(m_troups);
        m_validateComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetValidePixelAsInterger());
        m_claimedComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetColorClaimed());
        m_populationComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetMovingTroup());
        m_populationStaticComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetStaticTroup());

        if (m_airDrops.Count > 1024)
            throw new NotImplementedException("Not coded yet");
        for (int i = 0; i < m_aidDropToProcess.Length; i++)
        {
            if (i<m_airDrops.Count)
                m_aidDropToProcess[i] = m_airDrops[i];
            else 
                m_aidDropToProcess[i].m_troupCount=0;
        }
        m_airDrops.Clear();
        m_airDropsBuffer.SetData(m_aidDropToProcess);

        for (int i = 0; i < 32; i++)
        {
            m_randomness[i] = UnityEngine.Random.value - 0.5f;
        }
        m_randomnessBuffer.SetData(m_randomness);
        for (int i = 0; i < 32; i++)
        {
            m_randomdirection[i] = (uint) (UnityEngine.Random.value*10f)%8;
        }
        m_randomnessDirectionBuffer.SetData(m_randomdirection);
        //SET VALIDATE COMPUTE SHADER AND PRODUCE

        // DROP AIR DROP TROP ON THE MAP
        kernel = m_shaderApplyAirdropTroup.FindKernel("CSMain");
        m_shaderApplyAirdropTroup.SetInt("m_width", m_width);
        m_shaderApplyAirdropTroup.SetInt("rx", (int)UnityEngine.Random.Range(0f,1280f));
        m_shaderApplyAirdropTroup.SetInt("ry", (int)UnityEngine.Random.Range(0f, 720f));
        m_shaderApplyAirdropTroup.SetBuffer(kernel, "m_aidDrops", m_airDropsBuffer);
        m_shaderApplyAirdropTroup.SetBuffer(kernel, "m_troupMovingOnMap", m_populationComputeBuffer);
        m_shaderApplyAirdropTroup.SetBuffer(kernel, "m_troupStaticOnMap", m_populationStaticComputeBuffer);
        m_shaderApplyAirdropTroup.SetBuffer(kernel, "m_colorTeamOnMap", m_claimedComputeBuffer);
        m_shaderApplyAirdropTroup.SetBuffer(kernel, "m_valideTerritory", m_validateComputeBuffer);
        m_shaderApplyAirdropTroup.SetBuffer(kernel, "m_randomness", m_randomnessBuffer);
        m_shaderApplyAirdropTroup.Dispatch(kernel, 32,32, 1);
        //32

        kernel = m_shaderExtractTroup.FindKernel("CSMain");
        m_shaderExtractTroup.SetInt("m_width", m_width);
        m_shaderExtractTroup.SetBuffer(kernel, "m_troupExtraction", m_extractedTroupOfMapBuffer);
        m_shaderExtractTroup.SetBuffer(kernel, "m_troupOnMap", m_populationComputeBuffer);
        m_shaderExtractTroup.SetBuffer(kernel, "m_validePixel", m_validateComputeBuffer);
        m_shaderExtractTroup.Dispatch(kernel, m_width / 8, m_height / 8, 1);


        kernel = m_shaderNaturalMove.FindKernel("CSMain");
        m_shaderNaturalMove.SetInt("m_width", m_width);
        m_shaderNaturalMove.SetInt("m_randomDirectionCount", m_randomdirection.Length);
        m_shaderNaturalMove.SetBuffer(kernel, "m_randomDirection", m_randomnessDirectionBuffer);
        m_shaderNaturalMove.SetBuffer(kernel, "m_troupExtracted", m_extractedTroupOfMapBuffer);
        m_shaderNaturalMove.SetBuffer(kernel, "m_troupMovingOnMap", m_populationComputeBuffer);
        m_shaderNaturalMove.SetBuffer(kernel, "m_troupStaticOnMap", m_populationStaticComputeBuffer);
        m_shaderNaturalMove.SetBuffer(kernel, "m_colorPerPixel", m_claimedComputeBuffer);
        m_shaderNaturalMove.SetBuffer(kernel, "m_validePixel", m_validateComputeBuffer);
        m_shaderNaturalMove.Dispatch(kernel, m_width / 8, m_height / 8, 1);

        m_populationComputeBuffer.GetData(m_movingTroup);
        m_populationStaticComputeBuffer.GetData(m_staticTroup);
        m_claimedComputeBuffer.GetData(m_claimedTeamId);
      

        m_pixelWarsState.m_mapState.m_movingTroupRegister = m_movingTroup;
        m_pixelWarsState.m_mapState.m_staticTroupRegister = m_staticTroup;
        m_pixelWarsState.m_mapState.m_claimedRegister = m_claimedTeamId;


    }

    private void OnDestroy()
    {
        if (m_claimedComputeBuffer != null)
            m_claimedComputeBuffer.Release();
        if (m_populationComputeBuffer != null)
            m_populationComputeBuffer.Release();
        if (m_validateComputeBuffer != null)
            m_validateComputeBuffer.Release();
    }
}
