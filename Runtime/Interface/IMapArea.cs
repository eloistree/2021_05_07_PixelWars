﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMapArea 
{

    IEnumerable<IMapLinkedPosition> GetTargetPositions();
    IMapDimension GetMapRefDimension();
}
