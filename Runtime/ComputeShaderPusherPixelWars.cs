﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputeShaderPusherPixelWars : MonoBehaviour
{

    public PixelWarsStateMono m_pixelWarsState;


    uint count;
    int width;
    int height;


    [Header("Population Regrow")]
    public int m_minTroupRegrowing;
    public int m_troupPerRegrow;
    public int m_minStaticTroupRegrowing;
    public int m_staticTroupPerRegrow;
    public ComputeShader m_shaderRegrow;
    public ComputeBuffer m_validateComputeBuffer;
    public ComputeBuffer m_claimedComputeBuffer;
    public ComputeBuffer m_populationComputeBuffer;
    public ComputeBuffer m_populationStaticComputeBuffer;


    [Header("Massive Destruction")]
    public ComputeShader m_shaderAtomic;
    public float m_airstrike = 0.01f;
    public float m_miniNuke = 0.03f;
    public float m_nuke = 0.05f;
    public float m_tzarNuke = 0.1f;

    public ComputeShader m_shaderVirus;
    public float m_warVirus = 0.05f;
    public float m_blackPlagueVirus = 0.1f;


    public void ApplyAirStrike(float left2RightPct, float bot2TopPct) => ApplyNuke(left2RightPct, bot2TopPct, m_airstrike);
    public void ApplyMiniNuke(float left2RightPct, float bot2TopPct) => ApplyNuke(left2RightPct, bot2TopPct, m_miniNuke);
    public void ApplyNuke(float left2RightPct, float bot2TopPct) => ApplyNuke(left2RightPct, bot2TopPct, m_nuke);
    public void ApplyTzarNuke(float left2RightPct, float bot2TopPct) => ApplyNuke(left2RightPct, bot2TopPct, m_tzarNuke);


    public void ApplyNuke(float left2RightPct, float bot2TopPct, float radiusPct)
    {
        // Remore all troup of a zone
        PourcentToPixelCoordinate center = new PourcentToPixelCoordinate(left2RightPct, bot2TopPct);
        int radius = (int)(radiusPct * (float)width);
        uint w = (uint)width;
        uint h = (uint)height;
        center.ApplyCircleOnPixels(ref w, ref h, ref radius, m_pixelWarsState.SetToZeroXY);
    }

    public void ApplyCovidMinimalist(float startPoint, float endPoint) => ApplyVirus(startPoint, endPoint, 0.001f);
    public void ApplyCovidRealist(float startPoint, float endPoint) => ApplyVirus(startPoint, endPoint, 0.01f);
    public void ApplyWarVirus(float startPoint, float endPoint) => ApplyVirus(startPoint, endPoint, m_warVirus);
    public void ApplyBlackPlague(float startPoint, float endPoint) => ApplyVirus(startPoint, endPoint, m_blackPlagueVirus);

    public void ApplyVirus(float startPoint, float endPoint, float mortalityPct)
    {
        // Put a viruse on the pixel of the map. 
        // Kill mortalityPourcent of troup when appearing on a pixel
        // when not pixel are contaminated anymore, virus diseapear.

    }




    private bool m_hasBeenInit;
    public void Regrow()
    {
        if (m_pixelWarsState == null ||
             m_pixelWarsState.m_mapState == null ||
             m_pixelWarsState.m_mapState.GetPixelCount() <= 0)
            return;

        int kernel;
        if (!m_hasBeenInit)
        {
            m_hasBeenInit = true;
            CreateBuffers(count);
        }
        LoadDataFromGame();

        kernel = m_shaderRegrow.FindKernel("CSMain");
        m_shaderRegrow.SetInt("m_width", width);
        m_shaderRegrow.SetInt("m_minTroupRegrowing", m_minTroupRegrowing);
        m_shaderRegrow.SetInt("m_unityByGrowing", m_troupPerRegrow);
        m_shaderRegrow.SetInt("m_minStaticTroupRegrowing", m_minStaticTroupRegrowing);
        m_shaderRegrow.SetInt("m_unityStaticByGrowing", m_staticTroupPerRegrow);
        m_shaderRegrow.SetBuffer(kernel, "m_movingTroupOnMap", m_populationComputeBuffer);
        m_shaderRegrow.SetBuffer(kernel, "m_staticTroupOnMap", m_populationStaticComputeBuffer);
        m_shaderRegrow.SetBuffer(kernel, "m_colorTeamOnMap", m_claimedComputeBuffer);
        m_shaderRegrow.SetBuffer(kernel, "m_valideTerritory", m_validateComputeBuffer);

        count = m_pixelWarsState.m_mapState.GetPixelCount();

        m_shaderRegrow.Dispatch(kernel, width / 8, height / 8, 1);

        m_populationComputeBuffer.GetData(m_pixelWarsState.m_mapState.m_movingTroupRegister);
        m_populationStaticComputeBuffer.GetData(m_pixelWarsState.m_mapState.m_staticTroupRegister);
    }

    private void LoadDataFromGame()
    {
        m_validateComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetValidePixelAsInterger());
        m_claimedComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetColorClaimed());
        m_populationComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetMovingTroup());
        m_populationStaticComputeBuffer.SetData(m_pixelWarsState.m_mapState.GetStaticTroup());
    }

    private void CreateBuffers(uint count)
    {
        count = m_pixelWarsState.m_mapState.GetPixelCount();
        width = (int)m_pixelWarsState.m_mapState.GetPixelWidth();
        height = (int)m_pixelWarsState.m_mapState.GetPixelHeight();
        m_validateComputeBuffer = new ComputeBuffer((int)count, sizeof(int));
        m_claimedComputeBuffer = new ComputeBuffer((int)count, sizeof(int));
        m_populationComputeBuffer = new ComputeBuffer((int)count, sizeof(int));
        m_populationStaticComputeBuffer = new ComputeBuffer((int)count, sizeof(int));

        
    
    }




    void OnDestroy()
    {

        if(m_validateComputeBuffer!=null)
            m_validateComputeBuffer.Release();

        if (m_claimedComputeBuffer != null)
            m_claimedComputeBuffer.Release();

        if (m_populationComputeBuffer != null)
            m_populationComputeBuffer.Release();

        if (m_populationStaticComputeBuffer != null)
            m_populationStaticComputeBuffer.Release();
    }
}
