﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PlayerWallets : MonoBehaviour
{
    public PixelWarsPlayerMetaInformationRegisterMono m_playerMetaData;

    public int m_index=0;
    public int m_displayCount=4;
    public float m_timeBetweenSwithch = 4;
    void Start()
    {
        InvokeRepeating("NextWallets", 1, m_timeBetweenSwithch);   
    }

    public string m_display = "";
    public Text m_textDisplayWallet;
    void NextWallets()
    {
        m_display = "";
       UnityPlayerMetaInformation [] metaInfo = m_playerMetaData.GetAllPlayersInfo();
        int d = m_displayCount;
        if (d >= metaInfo.Length)
            d = metaInfo.Length;
        for (int i = 0; i < d ; i++)
        {
            if (m_index < metaInfo.Length) { 
                m_display += string.Format(" {0}|{1} ", metaInfo[m_index].m_playerId.m_name, metaInfo[m_index].m_wallet.GetCurrentWallet());
            }
            m_index++;
            if (m_index >= metaInfo.Length) {
                m_index = 0;
            }
        }
        if (m_display != null)
            m_textDisplayWallet.text = m_display;
    }
}
