﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelWarsSaveAndLoadAtUnityDataDirectory : MonoBehaviour
{

    public PixelWarsSaveAndLoad m_saveAndLoad;
    public string m_gameDirectoryName="PixelWars";
    public bool m_wasLoaded;
    public bool m_wasSaved;
    public void OnEnable()
    {
        Invoke("LoadPreviousGameInfo", 3);
    }

    private void LoadPreviousGameInfo()
    {
        Debug.Log("Load previous Game");
        m_saveAndLoad.LoadGame(GetPath());
        m_wasLoaded = true;
    }

    public void OnDisable()
    {
        if (!m_wasSaved)
        {
            SaveCurrentGame();
        }
    }
    [ContextMenu("Save As File")]
    private void SaveCurrentGame()
    {
        Debug.Log("Save current Game");
        m_saveAndLoad.SaveGame(GetPath());
        m_wasSaved = true;
    }

    public void OnDestroy()
    {
        if (!m_wasSaved)
        {
            SaveCurrentGame();
        }
    }
    public void OnApplicationExit()
    {

        if (!m_wasSaved)
        {
            SaveCurrentGame();
        }
    }

    private string GetPath()
    {
        return Application.persistentDataPath + "/" + m_gameDirectoryName;
    }

    [ContextMenu("Open Directory")]
    public void OpenDirectoryUsed() { Application.OpenURL(GetPath()); }

}
