﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextEventListenerToAction : MonoBehaviour
{

    public PixelWarsEntryTextForPlayer m_source;
    public PixelWarsPlayerCursorManager m_cursorManager;
    public PixelWarsPlayerRegisterMono m_playerInGame;

    public PixelWarsPlayerMetaInformationRegisterMono m_playerWalletInGame;
    public PixelWarsPlayerRequestInterface m_pixelWarsInterface;
    public PauseWorldRotationForFocus m_focusManager;

    void Start()
    {
        m_source.m_cursorMoveListener += MoveCursor;
        m_source.m_cursorColorListener += SetColorCursor;
        m_source.m_aidDropAtcursorListerner += AirDropAtCursor;
        m_source.m_fullAirDropAtcursorListerner += AirDropAtCursorFullFlush;
        m_source.m_createPlayer += CreatePlayer;
        m_source.m_nukeAtCursorListener += NukeAtCursor;
        m_source.m_laserStrikeListener += LaserStrikeAtPosition;
        m_source.m_laserStrikeListenerAtCursor += LaserStrikeOnCursor;
        m_source.m_cursorTranslate += TranslateCursor;
        m_source.m_defenseAtcursorListerner += DefenseAtCursor;
        m_source.m_cameraFocus += FocusCamera;

    }

    private void FocusCamera(string playerNameId,  float latitude, float longitude, float focusInPourcent)
    {
        FeetAllInformation(playerNameId, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll);
        if (foundAll)
        {
            m_focusManager.Add(new PauseWorldRotationForFocus.FocusInQueue(10f, longitude,latitude, focusInPourcent));

           // m_pixelWarsInterface.RequestDefense(id, cursor, meta.m_wallet, troupToSend, radiusInPixel);
        }
    }

    private void DefenseAtCursor(string playerNameId, uint troupToSend, int radiusInPixel)
    {
        FeetAllInformation(playerNameId, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll);
        if (foundAll)
        {

            m_pixelWarsInterface.RequestDefense(id, cursor, meta.m_wallet, troupToSend, radiusInPixel);
        }
    }

    private void TranslateCursor(string playerNameId, float l2rPct, float b2tPct)
    {
        FeetAllInformation(playerNameId, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll);
        if (foundAll)
        {
            l2rPct += cursor.GetLeft2RightPct();
            b2tPct += cursor.GetBot2TopPct();
            l2rPct = Mathf.Clamp(l2rPct, 0f, 1f);
            b2tPct = Mathf.Clamp(b2tPct, 0f, 1f);
            cursor.SetPosition(l2rPct, b2tPct);
        }
    }

    private void LaserStrikeOnCursor(string playerNameId)
    {
        FeetAllInformation(playerNameId, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll);
        if (foundAll)
        {
            m_pixelWarsInterface.RequestLaserStrike(id,cursor, meta.m_wallet);
        }
    }

    private void LaserStrikeAtPosition(string playerNameId, PourcentToPixelCoordinate where)
    {
        FeetAllInformation(playerNameId, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll);
        if (foundAll)
        {
            m_pixelWarsInterface.RequestLaserStrike(id,where, meta.m_wallet);
        }
    }

    private void OnDestroy()
    {
        m_source.m_cursorMoveListener -= MoveCursor;
        m_source.m_cursorColorListener -= SetColorCursor;
        m_source.m_aidDropAtcursorListerner -= AirDropAtCursor;
        m_source.m_fullAirDropAtcursorListerner -= AirDropAtCursorFullFlush;
        m_source.m_createPlayer -= CreatePlayer;
        m_source.m_nukeAtCursorListener -= NukeAtCursor;

    }

    public void FeetAllInformation(string playerName, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll)
    {
        m_playerInGame.m_register.HasPlayer(playerName, out bool f1, out id);
        m_cursorManager.HasCursor(id, out bool f2, out cursor);
        m_playerWalletInGame.HasMeta(id, out bool f3, out meta);
        foundAll = f1 && f2 && f3;

    }

    private void NukeAtCursor(string playerNameId, NukeType nuke)
    {
        FeetAllInformation(playerNameId, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll);
        if (foundAll) {
            m_pixelWarsInterface.RequestNuke(cursor,nuke, meta.m_wallet);
        }




    }

    private TroupWallet GetPlayerWallet(PlayerNameId playerName)
    {
        m_playerWalletInGame.GetWallet(playerName, out bool tmp, out TroupWallet wallet);
        return wallet;
    }

    private void AirDropAtCursor(string playerNameId, uint troupToSend)
    {
        FeetAllInformation(playerNameId, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll);
        if (foundAll)
        {
                    m_pixelWarsInterface.RequestAirDrop(cursor, new AirDropOnceBean(troupToSend, cursor.GetCurrentTeamTarget()), meta.m_wallet);
        }   
         
    }
    private void AirDropAtCursorFullFlush(string playerNameId)
    {

        FeetAllInformation(playerNameId, out PlayerNameId id, out UnityPlayerCursorInformation cursor, out UnityPlayerMetaInformation meta, out bool foundAll);
        if (foundAll)
        {
            m_pixelWarsInterface.RequestAirDropFullFlush(cursor, cursor.GetCurrentTeamTarget(), meta.m_wallet);
        }
            
    }



    private void SetColorCursor(string playerNameId, TeamColor color)
    {
        m_playerInGame.m_register.HasPlayer(playerNameId, out bool found, out PlayerNameId playerName);
        if (found)
        {
            m_cursorManager.HasCursor(playerName, out bool foundCursor, out UnityPlayerCursorInformation cursorFound);
            if (foundCursor) {

                cursorFound.SetCurrentColor(color);
            }
        }
    }

    private void MoveCursor(string playerNameId, float l2rPct, float b2tPCt)
    {
        m_playerInGame.m_register.HasPlayer(playerNameId, out bool found, out PlayerNameId playerName);
        if (found)
        {
            m_cursorManager.TryMoveCursorOrCreate(playerName, l2rPct, b2tPCt);
        }
    }



    private void CreatePlayer(string playerNameId, TeamColor color)
    {
        CreateOrCheckThatPlayerIsCreated(playerNameId);
        SetColorCursor(playerNameId, color);
    }

    public uint m_startTroup=50000;
    public void CreateOrCheckThatPlayerIsCreated(PlayerNameId playerId)
    {
        m_playerWalletInGame.CreateMetaFor(playerId);
        m_cursorManager.CreateCursorFor(playerId);
        m_playerWalletInGame.GetWallet(playerId, out bool foundWallet, out TroupWallet wallet);
        wallet.Add(m_startTroup);
    }
    public void CreateOrCheckThatPlayerIsCreated(string  playerId)
    {

        m_playerInGame.m_register.HasPlayer(playerId, out bool found, out PlayerNameId playerName);
        if (!found) {
            m_playerInGame.m_register.AddPlayer(playerId, out playerName);
        }
        CreateOrCheckThatPlayerIsCreated(playerName);
    }



}
