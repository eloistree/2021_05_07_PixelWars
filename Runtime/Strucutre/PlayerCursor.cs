﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class PlayerCursorWithPosition : PlayerCursor {

    public float m_left2Right=0.5f;
    public float m_bot2Top=0.5f;

}

[System.Serializable]
public class PlayerCursor
{
    public uint m_playerId;
    public TeamColor m_teamColorId;
    public uint m_troupAvailable;
    public uint m_troupPerTurn;
    public bool m_isActive;

    public void AddTroup(uint troup) { m_troupAvailable += troup; }
    public void SetTroup(uint troup) { m_troupAvailable = troup; }
    public void SetPushByTurn(uint troup) { m_troupPerTurn = troup; }



    public uint DequeueTroup() {
        if (m_isActive == false)
            return 0;

        if (m_troupAvailable <= 0)
            return 0;
        else if (m_troupAvailable > m_troupPerTurn) {
            m_troupAvailable -= m_troupPerTurn;
            return m_troupPerTurn;
        }else {
            uint lastTroup = m_troupAvailable;
            m_troupAvailable = 0;
            return lastTroup;
        }
    }
}

[System.Serializable]
public class TeamColor {

    public Color m_teamColor;
    public uint m_colorAsIntegerId;

    public TeamColor(Color teamColor)
    {
        m_teamColor = teamColor;
        m_colorAsIntegerId = TeamColor.GetIdFrom(teamColor);
    }

    public static uint GetIdFrom(Color teamColor)
    {
        return uint.Parse(string.Format("{0:000}{1:000}{2:000}", teamColor.r * 255f, teamColor.g * 255f, teamColor.b * 255f));
    }

    public uint GetColorId() { return m_colorAsIntegerId; }
    public Color GetColor() { return m_teamColor; }

    public static TeamColor GetFrom(Color color)
    {
        return new TeamColor(color);
    }
    public static TeamColor GetRandom() {
        return GetIdFrom(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
    }
    public static TeamColor GetIdFrom(uint r, uint g, uint b)
    {
        return GetIdFrom((float)r, (float)g, (float)b);
    }
    public static TeamColor GetIdFrom(float r, float g, float b)
    {
        if (r > 1f)
            r = r / 255f;
        if (g > 1f)
            g = g / 255f;
        if (b > 1f)
            b = b / 255f;
        return new TeamColor(new Color(r , g , b ));
    }

    public static TeamColor GetFromUint(string id)
    {
        uint.TryParse(id, out uint parseId);
        return GetFromUint(parseId);

    }
    public static TeamColor GetFromUint(uint id)
    {
      double  m_r = (id) * 0.000001;
      double  m_g = (id * 0.001);
        double m_b = (id);

        uint  m_ri = (uint)(((double)id) * 0.000001);
        uint  m_gi = (uint)(((double)id) * 0.001 - (1000.0 * (int)m_r));
        uint m_bi = (uint)(((double)id) - (1000.0 * (int)m_g));
    

        return GetIdFrom(m_ri, m_gi, m_bi);

    }

    public static TeamColor CreateFrom(int r255, int g255, int b255)
    {
        return new TeamColor(new Color(r255 / 255f, g255 / 255f, b255 / 255f));
    }
}