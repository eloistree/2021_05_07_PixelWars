﻿public class MapDimensionDefault : IMapDimension
{
    uint m_width;
    uint m_height;
    uint m_count;
    V2ReadingDirection m_readingDirection;

    public MapDimensionDefault()
    {
        m_height = m_width = m_count = 0;
        m_readingDirection = V2ReadingDirection.Undefined;
    }

    public MapDimensionDefault(uint width, uint height, V2ReadingDirection readingDirection = V2ReadingDirection.L2RT2B)
    {
        m_width = width;
        m_height = height;
        m_count = width * height;
        m_readingDirection = readingDirection;
    }

    public void GetCount(out uint count)
    {
        count = m_count;
    }
    public void GetCount(out int count)
    {
        count = (int)m_count;
    }

    public uint GetCount()
    {
        return m_count;
    }

    public void GetHeight(out uint height)
    {

        height = m_height;
    }

    public void GetHeight(out int height)
    {

        height = (int)m_height;
    }

    public uint GetHeight()
    {

        return m_height;
    }

    public V2ReadingDirection GetMapReadingDirection()
    {
        return m_readingDirection;
    }

    public void GetWidth(out uint width)
    {
        width = m_width;
    }

    public void GetWidth(out int width)
    {
        width = (int)m_width;
    }

    public uint GetWidth()
    {
        return m_width;
    }

    public bool IsDimensionNotZero()
    {
        return m_width != 0 && m_height != 0;
    }
}

