﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public class PixelWarsSaveAndLoad : MonoBehaviour
{

    public string m_folderLocationDebug;
    public string m_lastNameSave="lastSave";

    public PixelWarsStateMono m_pixelWarsState;
    public PixelWarsPlayerRegisterMono m_playerRegister;
    public ComputeShaderGetterPixelWars m_computedTexture;
    public PixelWarsPlayerMetaInformationRegisterMono m_playerInformation;

    public string m_validePixel="ValidePixel.txt";
    public string m_claimedPerPixel="ClaimedPerPixel.txt";
    public string m_movingTroupPerPixel="MovingTroupPerPixel.txt";
    public string m_staticTroupPerPixel="StaticTroupPerPixel.txt";
    public string m_civilTroupPerPixel= "CivilTroupPerPixel.txt";
    public string m_playersInformationFilName = "PlayersInformation.json";
    public string m_gameInformationFileName = "GameInformation.json";

    [Header("Loaded Debug")]
    public bool m_succedToLoadPlayersInformation;
    public PlayersBackupInformation m_playersInformationBackup;
    public bool m_succedToLoadGamesInformation;
    public GameInformation m_gameInformationBackup;

    public void LoadGame(string absoluteDirectoryPath)
    {
        m_folderLocationDebug = absoluteDirectoryPath;
        string json;
        int pixelCount=0;

        GetFile(m_gameInformationFileName, out json);
        try
        {

            m_gameInformationBackup = JsonUtility.FromJson<GameInformation>(json);
            m_succedToLoadGamesInformation = true;
            m_pixelWarsState.m_mapState= new PixelWarsState(m_gameInformationBackup.m_width, m_gameInformationBackup.m_height);
            pixelCount = (int)(m_gameInformationBackup.m_width * m_gameInformationBackup.m_height);
        }
        catch (Exception e) {
            Debug.Log("EXCEPTION:" + e.StackTrace);
            return; }

        GetFile(m_playersInformationFilName, out  json);
        try
        {

            Debug.Log("TEST:" + json);
            m_playersInformationBackup = JsonUtility.FromJson<PlayersBackupInformation>(json);
            m_succedToLoadPlayersInformation = true;

            foreach (PlayerBackupBean item in m_playersInformationBackup.m_playersInGame)
            {
                PlayerNameId pid = new PlayerNameId(item.m_playerName, item.m_playerId);
                m_playerRegister.m_register.AddOrOverride(pid);
                m_playerInformation.CreateOrOverridMetaFor(pid);
                m_playerInformation.GetWallet(pid, out bool found, out TroupWallet wallet);
                if (found) {
                    if (decimal.TryParse(item.m_wallet, out decimal value))
                        wallet.SetTo(value);
                }

            }
            ///DO STUFF
        }
        catch (Exception e) {

            Debug.Log("EXCEPTION:" + e.StackTrace);
            //return; 
        }

        string txt = "";
        string [] tokens ;
        int[] intArray;
        GetFile(m_validePixel, out  txt);
        tokens = txt.Split('|');
        intArray= new int[tokens.Length];
        for (int i = 0; i < tokens.Length; i++)
        {
            if (string.IsNullOrEmpty(tokens[i]))
                intArray[i] = 0;
            else
                intArray[i] = int.Parse(tokens[i]);
        }
        m_pixelWarsState.m_mapState.SetValidePixel(intArray.ToArray());


        GetFile(m_claimedPerPixel, out txt);
        tokens = txt.Split('|');
        intArray = new int[tokens.Length];
        for (int i = 0; i < tokens.Length; i++)
        {
            if (string.IsNullOrEmpty(tokens[i]))
                intArray[i] = 0;
            else
                intArray[i] = int.Parse(tokens[i]);
        }
        m_pixelWarsState.m_mapState.SetClaimed(intArray.ToArray());


       GetFile(m_movingTroupPerPixel, out txt);
        tokens = txt.Split('|');
        intArray = new int[tokens.Length];
        for (int i = 0; i < tokens.Length; i++)
        {
            if (string.IsNullOrEmpty(tokens[i]))
                intArray[i] = 0;
            else
                intArray[i] = int.Parse(tokens[i]);
        }
        m_pixelWarsState.m_mapState.SetMovingTroup(intArray.ToArray());


        GetFile(m_staticTroupPerPixel, out txt);
        tokens = txt.Split('|');
        intArray = new int[tokens.Length];
        for (int i = 0; i < tokens.Length; i++)
        {
            if (string.IsNullOrEmpty(tokens[i]))
                intArray[i] = 0;
            else
                intArray[i] = int.Parse(tokens[i]);
        }
        m_pixelWarsState.m_mapState.SetStaticMove(intArray.ToArray());



        GetFile(m_civilTroupPerPixel, out txt);
        tokens = txt.Split('|');
        intArray = new int[tokens.Length];
        for (int i = 0; i < tokens.Length; i++)
        {
            if (string.IsNullOrEmpty(tokens[i]))
                intArray[i] = 0;
            else
                intArray[i] = int.Parse(tokens[i]);
        }
        m_pixelWarsState.m_mapState.SetCivilTroup(intArray.ToArray());



    }


    public void SaveGame(string absoluteDirectoryPath)
    {
        m_folderLocationDebug = absoluteDirectoryPath;
        if (!Directory.Exists(m_folderLocationDebug))
        {
            Directory.CreateDirectory(m_folderLocationDebug);
        }


        string fileNameWithExt = "";
        StringBuilder textBuilder = new StringBuilder();

        string text = "";
        fileNameWithExt = m_claimedPerPixel;
        text = Join("|", m_pixelWarsState.m_mapState.m_claimedRegister);
        SaveFile(fileNameWithExt, text);

        fileNameWithExt = m_movingTroupPerPixel;
        text = Join("|", m_pixelWarsState.m_mapState.m_movingTroupRegister);
        SaveFile(fileNameWithExt, text);

        fileNameWithExt = m_staticTroupPerPixel;
        text = Join("|", m_pixelWarsState.m_mapState.m_staticTroupRegister);
        SaveFile(fileNameWithExt, text);

        fileNameWithExt = m_validePixel;
        text = Join("|", m_pixelWarsState.m_mapState.m_validePixelRegisterAsInteger);
        SaveFile(fileNameWithExt, text);


        fileNameWithExt = m_playersInformationFilName;
        m_playersInformationBackup = GetBackupInformation();
        text = JsonUtility.ToJson(m_playersInformationBackup);
        SaveFile(fileNameWithExt, text);

        fileNameWithExt = m_gameInformationFileName;
        m_gameInformationBackup = GetGameInformation();
        text = JsonUtility.ToJson(m_gameInformationBackup);
        SaveFile(fileNameWithExt, text);



        //SaveTexture(m_pixelWarsState.m_mapValidityUsed, m_folderLocationDebug, "MapUsed");
        //SaveTexture(m_computedTexture.m_claimedMapBUffer, m_folderLocationDebug, "Claimed");
        //SaveTexture(m_computedTexture.m_populationMapBuffer, m_folderLocationDebug, "MovingTroup");
        //SaveTexture(m_computedTexture.m_populationStaticMapBuffer, m_folderLocationDebug, "StaticTroup");

    }

    StringBuilder builder = new StringBuilder();
    private string Join(string spliter, int[] array)
    {
        builder.Clear();
        for (int i = 0; i < array.Length; i++)
        {
            builder.Append( array[i] == 0 ? "" :array[i].ToString());
            if(i<array.Length-1)
               builder.Append(spliter);

        }
        return builder.ToString();
    }

    private GameInformation GetGameInformation()
    {
       return  new GameInformation(m_pixelWarsState.m_mapState.m_width, m_pixelWarsState.m_mapState.m_height);
    }

    private void SaveFile(string fileNameWithExt, string text)
    {
        File.WriteAllText(GetPath(fileNameWithExt), text);
    }
    private void GetFile(string fileNameWithExt, out string text)
    {
        if (!File.Exists(GetPath(fileNameWithExt)))
            text= "";
        else 
            text = File.ReadAllText(GetPath(fileNameWithExt));
    }

    private string GetPath(string fileNameWithExt)
    {
        return m_folderLocationDebug + "/" + fileNameWithExt;
    }

    public PlayersBackupInformation GetBackupInformation()
    {
        PlayersBackupInformation info = new PlayersBackupInformation();
        for (int i = 0; i < m_playerInformation.m_playersMetaInfoInGame.Count; i++)
        {
            info.m_playersInGame.Add(new PlayerBackupBean(
                m_playerInformation.m_playersMetaInfoInGame[i].m_wallet.GetCurrentWallet(),
                m_playerInformation.m_playersMetaInfoInGame[i].m_playerId.m_name,
                m_playerInformation.m_playersMetaInfoInGame[i].m_playerId.m_inGameId
                ));
        }

        return info;
    }

    private void SaveTexture(Texture2D texture, string path, string nameFileNoExtension)
    {
        byte[] bytes = texture.EncodeToPNG();
        var dirPath = Application.dataPath + "/RenderOutput";
        if (!System.IO.Directory.Exists(dirPath))
        {
            System.IO.Directory.CreateDirectory(dirPath);
        }
        System.IO.File.WriteAllBytes(path + "/" + nameFileNoExtension + ".png", bytes);
//#if UNITY_EDITOR
//        UnityEditor.AssetDatabase.Refresh();
//#endif
    }


    private void SaveTexture(RenderTexture texture, string path, string nameFileNoExtension) { 
        SaveTexture(toTexture2D(texture), path, nameFileNoExtension);
    }

        Texture2D toTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGB24, false);
        // ReadPixels looks at the active RenderTexture.
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }
}


[System.Serializable]
public class PlayersBackupInformation
{
    public List<PlayerBackupBean> m_playersInGame = new List<PlayerBackupBean>();

    internal List<PlayerNameId> GetPlayerList()
    {
        throw new NotImplementedException();
    }
}
[System.Serializable]
public class PlayerBackupBean
{
    public string m_playerName;
    public string m_wallet;
    public int m_playerId;
    public PlayerBackupBean()
    {
        m_wallet = "";
        m_playerName = "";
        m_playerId = 0;
    }
    public PlayerBackupBean(decimal wallet, string playerName, int playerId)
    {
        m_wallet = wallet.ToString();
        m_playerName = playerName;
        m_playerId = playerId;
    }
}


[System.Serializable]
public class GameInformation {
    public uint m_width;
    public uint m_height;
    public string m_saveDate;
    public void SetSaveDate() => m_saveDate = DateTime.Now.ToString();

    public GameInformation(uint width, uint height)
    {
        m_width = width;
        m_height = height;
    }
}