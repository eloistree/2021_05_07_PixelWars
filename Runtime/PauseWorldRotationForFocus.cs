﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseWorldRotationForFocus : MonoBehaviour
{
    public CameraWorldFocusTransition m_transition;
    public CameraWorldFocusAutoRotate m_autoRotation;
    private FocusInQueue m_currentFocus;
    public List<FocusInQueue> m_focusInQueue = new List<FocusInQueue>();
    public bool m_autoRun;

    [System.Serializable]
    public class FocusInQueue {
        public float m_allocatedTime;
        public float m_longitude;
        public float m_latitude;
        public float m_cameraFocus;

        public FocusInQueue(float allocatedTime, float longitude, float latitude, float cameraFocus)
        {
            m_allocatedTime = allocatedTime;
            m_longitude = longitude;
            m_latitude = latitude;
            m_cameraFocus = cameraFocus;
        }
    }

    public void Add(FocusInQueue focus) {
        m_focusInQueue.Add(focus);
    }
    void Update()
    {
        if (m_autoRun)
            m_autoRotation.enabled = m_currentFocus == null && m_focusInQueue.Count == 0;
        else 
            m_autoRotation.enabled = false;
        if (m_currentFocus == null && m_focusInQueue.Count > 0)
        {
            FocusNextInLine();
        }

        if (m_currentFocus!=null) {
            m_currentFocus.m_allocatedTime -= Time.deltaTime;
            if (m_currentFocus.m_allocatedTime < 0f) {
                if (m_focusInQueue.Count>0)
                {
                    FocusNextInLine();
                }
                else {
                    m_currentFocus = null;
                }
            }
        
        }
    }

    private void FocusNextInLine()
    {
        m_currentFocus = m_focusInQueue[0];
        m_focusInQueue.RemoveAt(0);
        m_autoRotation.enabled = false;
        m_transition.m_wantedLatitude = m_currentFocus.m_latitude;
        m_transition.m_wantedLongitude = m_currentFocus.m_longitude;
        m_transition.m_wantedFocusInPourcent = m_currentFocus.m_cameraFocus;
    }
}
