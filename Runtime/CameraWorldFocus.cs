﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWorldFocus : MonoBehaviour
{
    public Transform m_anchor;
    public Transform m_worldToRotate;
    public Camera m_targetCamera;
    public float m_minCameraFocus = 0.1f;
    public float m_maxCameraFocus = 1;


    [Header("Debug")]
    public float m_latitude=0;
    public float m_longitude=0;
    public float m_cameraFocus;
    public float m_cameraFocusInPourcent;



    public void RefreshTransformPosition() {

        m_targetCamera.orthographicSize = m_cameraFocus;
        Quaternion rotation = m_anchor.rotation;
        m_worldToRotate.rotation = (Quaternion.Euler(-m_longitude, 0, 0) * Quaternion.Euler(0,m_latitude, 0))   *  rotation;
    }
    public void SetPosition(float latitude, float longitude, float focusMaxPourcent = 1f) {
        SetLatitude(latitude);
        SetLongitude(longitude);
        SetFocusAsPourcent(focusMaxPourcent);
        RefreshTransformPosition();


    }

    public void SetFocus(float focus)
    {
        m_cameraFocus = Mathf.Clamp(focus, m_minCameraFocus, m_maxCameraFocus);
    }
    public void SetFocusAsPourcent(float focusPourcent)
    {
        m_cameraFocusInPourcent = focusPourcent;
        m_cameraFocus = m_minCameraFocus + (m_maxCameraFocus - m_minCameraFocus) * focusPourcent;
    }

    public void SetLongitude(float longitude)
    {
        m_longitude = Mathf.Clamp(longitude, -90f, 90f);
    }

    public void SetLatitude(float latitude)
    {
        m_latitude = Mathf.Clamp(latitude, -180, 180);
    }

    private void OnValidate()
    {
        SetLatitude(m_latitude);
        SetLongitude(m_longitude);
        SetFocus(m_cameraFocus);
        RefreshTransformPosition();
    }
}
