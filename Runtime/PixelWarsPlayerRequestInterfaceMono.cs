﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelWarsPlayerRequestInterfaceMono : PixelWarsPlayerRequestInterface
{

    [Header("Bad code")]
    public ComputeShaderPusherPixelWars m_computerPusher;
    public PixelWarsStateMono m_pixelWarsInterface;


    public uint m_costMultiplicator=1;

    [Header("Laser")]
    public uint m_costLaserStrike = 100;

    [Header("Nuke")]
    public uint m_costNukeH = 100000;
    public uint m_costNukeAtomic = 10000;
    public uint m_costNukeMini = 5000;
    public uint m_costNukeAirstrike = 1000;




    public override void RequestAirDrop(UnityPlayerCursorInformation playerCursor, AirDropOnceBean airdrop, TroupWallet wallet)
    {
        if (!IsPixelValide(playerCursor))
            return;
        uint troup = airdrop.m_troup;
        troup = wallet.Claim(troup);

        if (troup > 0) {
            m_pixelWarsInterface.m_mapState.AddTroup(
                playerCursor.GetLeft2RightPct(),
                playerCursor.GetBot2TopPct(),
                (int)troup, TroupType.Moving,
                airdrop.m_teamOfTheDrop.GetColor()  
                );     
        }
    }

    public override void RequestAirDropFullFlush(UnityPlayerCursorInformation playerCursor,TeamColor airdropTeamOwner, TroupWallet wallet)
    {
        if (!IsPixelValide(playerCursor))
            return;
        wallet.ClaimAll(out int troup);
        if (troup > 0)
        {
            m_pixelWarsInterface.m_mapState.AddTroup(
                playerCursor.GetLeft2RightPct(),
                playerCursor.GetBot2TopPct(),
                (int)troup, TroupType.Moving,
                airdropTeamOwner.GetColor()
                );
        }
    }

    private bool IsPixelValide(UnityPlayerCursorInformation playerCursor)
    {
        return IsPixelValide(playerCursor.m_cursorMirror);
    }
    private bool IsPixelValide(PlayerCursorForShader playerCursor)
    {
        return m_pixelWarsInterface.IsValide(playerCursor.m_left2RightPourcent, playerCursor.m_bot2TopPourcent);
    }

    private bool IsPixelValide(float left2RightPourcent, float bot2TopPourcent)
    {
        return m_pixelWarsInterface.IsValide(left2RightPourcent, bot2TopPourcent);
    }


    public override void RequestDoomStrike(UnityPlayerCursorInformation playerCursor, FUckThisAreaType actionType, TroupWallet wallet)
    {
        throw new System.NotImplementedException();
    }

   

    public override void RequestNuke(UnityPlayerCursorInformation playerCursor, NukeType actionType, TroupWallet wallet)
    {
        decimal money = wallet.GetCurrentWallet();
        if (actionType == NukeType.Airstrike && money >= m_costNukeAirstrike)
        {
            wallet.Claim(m_costNukeAirstrike);
            m_computerPusher.ApplyAirStrike(playerCursor.GetLeft2RightPct(), playerCursor.GetBot2TopPct());
        }
        else if (actionType == NukeType.Atomic && money >= m_costNukeAtomic)
        {
            wallet.Claim(m_costNukeAtomic);
            m_computerPusher.ApplyNuke(playerCursor.GetLeft2RightPct(), playerCursor.GetBot2TopPct());
        }
        else if (actionType == NukeType.H && money >= m_costNukeH)
        {
            wallet.Claim(m_costNukeH);
            m_computerPusher.ApplyTzarNuke(playerCursor.GetLeft2RightPct(), playerCursor.GetBot2TopPct());
        }
        else if (actionType == NukeType.Mini && money >= m_costNukeMini)
        {
            wallet.Claim(m_costNukeMini);
            m_computerPusher.ApplyMiniNuke(playerCursor.GetLeft2RightPct(), playerCursor.GetBot2TopPct());
        }
    }

    public override void RequestVirus(UnityPlayerCursorInformation playerCursor, VirusType actionType, TroupWallet wallet)
    {
        throw new System.NotImplementedException();
    }

    public override void SetContinusAutoAirDrop(UnityPlayerCursorInformation playerCursor, AutoAirDropBean autoAirdrop, TroupWallet wallet)
    {
        throw new System.NotImplementedException();
    }

    public override void RequestLaserStrike(UnityPlayerCursorInformation playerCursor, TroupWallet wallet)
    {
        
        RequestLaserStrike(playerCursor.m_playerId, playerCursor.GetPctPixel(), wallet);
    }

    public override void RequestLaserStrike(PlayerNameId id, UnityPlayerCursorInformation cursor, TroupWallet wallet)
    {

        RequestLaserStrike(id, cursor.GetPctPixel(), wallet);
    }

    public override void RequestLaserStrike(PlayerNameId id, PourcentToPixelCoordinate where, TroupWallet wallet)
    {
        if (!IsPixelValide(where.GetLeft2Right(), where.GetBot2Top()))
            return;
        if (wallet.GetCurrentWallet() >= m_costLaserStrike)
        {
            wallet.Claim(m_costLaserStrike);
            m_pixelWarsInterface.SetToZero(where.GetLeft2Right(), where.GetBot2Top());
        }
    }


    private void ResetToZeroPixel(PlayerCursorForShader playerCursor)
    {

        m_pixelWarsInterface.SetToZero(playerCursor.m_left2RightPourcent, playerCursor.m_bot2TopPourcent);
    }

    public override void RequestDefense(PlayerNameId id, UnityPlayerCursorInformation cursor, TroupWallet wallet, uint troupToSend, int radiusInPixel)
    {
        PourcentToPixelCoordinate where= cursor.GetPctPixel();
        if (!IsPixelValide(where.GetLeft2Right(), where.GetBot2Top()))
            return;

        if (wallet.GetCurrentWallet() >= troupToSend)
        {

            uint w = m_pixelWarsInterface.m_mapState.GetPixelWidth();
            uint h = m_pixelWarsInterface.m_mapState.GetPixelHeight();
            where.GetPixelsInCircleAround(ref w, ref h, ref radiusInPixel, out List<PixelL2RB2T> list);
            if (list.Count > 0) {
                wallet.Claim(troupToSend);

                Shuffle<PixelL2RB2T> (list);

                // MAYBE IF TROUP <50 quick random spreading if troup >bignumber split per group
                // FOR optimiation purpose;
                    int i = 0;
                    PixelL2RB2T lastValide;
                    where.GetAsPixel(ref w, ref h, out lastValide);
                   while (troupToSend>0 && i<=troupToSend)
                    {
                        PixelL2RB2T s = list[i % list.Count];
                        if(m_pixelWarsInterface.IsValidePx((int)s.m_l2r,(int) s.m_b2t))
                        { 
                            m_pixelWarsInterface.AddDefense(1,(int)cursor.GetCurrentTeamTarget().m_colorAsIntegerId , s.m_l2r, s.m_b2t);
                            lastValide = s;
                            troupToSend--;
                        }
                        i++;
                    }
                    m_pixelWarsInterface.AddDefense((int)troupToSend, (int) cursor.GetCurrentTeamTarget().m_colorAsIntegerId, lastValide.m_l2r, lastValide.m_b2t);
            }
        }

    }
    private static System.Random rng = new System.Random();

    public static void Shuffle<T>( IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}



