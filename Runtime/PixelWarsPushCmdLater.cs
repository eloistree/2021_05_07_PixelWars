﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PixelWarsPushCmdLater : MonoBehaviour
{

    [System.Serializable]
    public class StringEvent : UnityEvent<string> { };
    public StringEvent m_ping;

    public List<TimeCmd> inWaiting = new List<TimeCmd>();
    [System.Serializable]
    public class TimeCmd
    {
        public float m_timeInSeconds;
        [TextArea(0, 5)]
        public string m_cmd;

    }
    void Update()
    {
        for (int i = inWaiting.Count - 1; i >= 0; i--)
        {
            inWaiting[i].m_timeInSeconds -= Time.deltaTime;
            if (inWaiting[i].m_timeInSeconds < 0)
            {

                string[] c = inWaiting[i].m_cmd.Split('\n');
                for (int j = 0; j < c.Length; j++)
                {
                    m_ping.Invoke(c[i]);
                }
                inWaiting.RemoveAt(i);
            }

        }



    }
}
