﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWorldFocusAutoRotate : MonoBehaviour
{
    public CameraWorldFocusTransition m_target;
    public float m_currentLatitude;
    public float m_uperLongitudeTweaking=10;
    public float m_latitudeSpeedAnglePerSecond=50;
    public float m_defaultFocusMax = 1;
    void Update()
    {

        m_currentLatitude += Time.deltaTime * m_latitudeSpeedAnglePerSecond;
        if (m_currentLatitude < -180f) { 
            m_currentLatitude = m_currentLatitude + 360f;
            m_target.OverrideLatitude(m_currentLatitude);
        }
        if (m_currentLatitude > 180f)
        { 
            m_currentLatitude =  m_currentLatitude-360;
            m_target.OverrideLatitude(m_currentLatitude);
        }


        m_target.m_wantedLongitude = m_uperLongitudeTweaking;
        m_target.m_wantedLatitude = m_currentLatitude;
        m_target.m_wantedFocusInPourcent = m_defaultFocusMax;
    }
}
