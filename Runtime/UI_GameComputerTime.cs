﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GameComputerTime : MonoBehaviour
{
    public Text m_time;

    // Update is called once per frame
    void Update()
    {

        m_time.text = string.Format("{0:00}:{1:00} {2:00}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second) ;
        
    }
}
