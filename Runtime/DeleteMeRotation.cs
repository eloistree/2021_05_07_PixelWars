﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteMeRotation : MonoBehaviour
{
    public Vector3 m_rotation = new Vector3(0,1,0);
    public float m_anglePerSecond=90;
    public Space m_space;

    void Update()
    {

        transform.Rotate(m_rotation * Time.deltaTime* m_anglePerSecond, m_space) ;
        
    }
}
