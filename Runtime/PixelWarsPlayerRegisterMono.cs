﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelWarsPlayerRegisterMono : MonoBehaviour
{

    public List<PlayerNameId> m_playersFromEditor;
    public PixelWarsPlayerRegister m_register= new PixelWarsPlayerRegister();

    void Awake()
    {
        m_register.OverridePlayerBase(m_playersFromEditor);
    }
}

public class PixelWarsPlayerRegister {

    public List<PlayerNameId> m_playerInGame = new List<PlayerNameId>();

    public void HasPlayer(int playerId, out bool playerFound, out PlayerNameId playerIdFound)
    {
        playerFound = false;
        playerIdFound = null;
        for (int i = 0; i < m_playerInGame.Count; i++)
        {
            if (m_playerInGame[i].m_inGameId == playerId)
            { 
                playerFound = true;
                playerIdFound = m_playerInGame[i];
                break;
            }
        }
    }

    public void HasPlayer(string playerName, out bool playerFound, out PlayerNameId playerIdFound)
    {
        playerFound = false;
        playerIdFound = null;
        for (int i = 0; i < m_playerInGame.Count; i++)
        {
            if (m_playerInGame[i].m_name == playerName)
            {
                playerFound = true;
                playerIdFound = m_playerInGame[i];
                break;
            }
        }
    }

    public Dictionary<string, int> m_playerIdByName = new Dictionary<string, int>();
    public Dictionary<int, string> m_playerIdById = new Dictionary<int,string>();


    public void AddPlayer(string player, out PlayerNameId created)
    {
        if (!m_playerIdByName.ContainsKey(player))
        {
            int id = CatchNotUsedInterger();
            AddPlayerWithoutQuestionning(player, id, out created);
        }
        else { 
            HasPlayer(player, out bool f, out created);
        }
    }

    private void AddPlayerWithoutQuestionning(string player, int id, out PlayerNameId created)
    {
        m_playerIdByName.Add(player, id);
        m_playerIdById.Add(id, player);
        created = new PlayerNameId(player, id);
        m_playerInGame.Add(created);
    }

    private int CatchNotUsedInterger()
    {
        int id = 0;
        bool idFound=false;
        while (!idFound) {
            id=   UnityEngine.Random.Range(0, int.MaxValue);
            idFound = ! m_playerIdById.ContainsKey(id);
        }
        return id;
    }

    internal void AddOrOverride(PlayerNameId pid)
    {
        HasPlayer(pid.m_name, out bool found, out PlayerNameId player);
        if (found)
        {
            player.m_name = pid.m_name;
            player.m_inGameId = pid.m_inGameId;
        }
        else {

            m_playerIdByName.Add(pid.m_name, pid.m_inGameId);
            m_playerIdById.Add(pid.m_inGameId, pid.m_name);
            m_playerInGame.Add(pid);
        }
    }


    public List<PlayerNameId> OverridePlayerBase()
    {
        return m_playerInGame;

    }

    public void OverridePlayerBase(List< PlayerNameId> players)
    {
        m_playerIdByName.Clear();
        m_playerIdById.Clear();
        for (int i = 0; i < players.Count; i++)
        {
            if(!string.IsNullOrEmpty(players[i].m_name) && players[i].m_inGameId>-1)
            AddPlayerWithoutQuestionning(players[i].m_name, players[i].m_inGameId, out PlayerNameId id);
        }

    }

}



[System.Serializable]
public class PlayerNameId {
    public string m_name;
    public int m_inGameId;

    public PlayerNameId(string name, int inGameId)
    {
        m_name = name;
        m_inGameId = inGameId;
    }

    public override bool Equals(object obj)
    {
        return obj is PlayerNameId id &&
               m_name == id.m_name &&
               m_inGameId == id.m_inGameId;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }

    internal string GetNameWithoutPlatform()
    {
        throw new NotImplementedException();
    }
}
