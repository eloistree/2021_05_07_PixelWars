﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelWarsProceduralLogicManager : MonoBehaviour
{


    public ComputeShaderPusherPixelWars m_computePusher;
    public ComputeShaderGetterPixelWars m_computeGetter;
    public UI_PixelWarsStatistic m_pixelWarsStatistic;

    public void ProcessTurn() {

        CapturePlayerRequest();
        PopulateTheMap();
        ProduceStaticTroupProduction();
        ExtractTroupThatNeedToBeMoved();
        MoveInTerritoryTroup();
        MoveAttackerTroup();
        ApplyFightAroundTheWorld();
        ApplyAirdropRequestAroundTheWorld();
        ApplyMasseDestructionRequestAroundtTheWorld();
        RenderingClaimedZone();
        RenderingBloodyBorder();
        RenderingPopulation();
        RenderingCurrentGameState();
        RefreshGameStatistic();

    }

    private void RefreshGameStatistic()
    {
        m_pixelWarsStatistic.Refresh();
    }

    public void PopulateTheMap() { }


    public void CapturePlayerRequest() { }
    public void ProduceStaticTroupProduction() { }
    public void ExtractTroupThatNeedToBeMoved() { }
    public void MoveInTerritoryTroup() { }
    public void MoveAttackerTroup() { }
    public void ApplyFightAroundTheWorld() { }
    public void ApplyAirdropRequestAroundTheWorld() { }
    public void ApplyMasseDestructionRequestAroundtTheWorld() { }

    public void RenderingClaimedZone() { }
    public void RenderingPopulation() { }

    public void RenderingBloodyBorder() { }
    public void RenderingCurrentGameState() { }



}
