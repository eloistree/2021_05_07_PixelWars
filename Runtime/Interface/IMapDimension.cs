﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMapDimension 
{
    V2ReadingDirection GetMapReadingDirection();
    void GetWidth(out uint width);
    void GetWidth(out int width);
    void GetHeight(out uint height);
    void GetHeight(out int height);
    void GetCount(out uint count);
    void GetCount(out int count);
    bool IsDimensionNotZero();

    uint GetWidth();
    uint GetHeight();
    uint GetCount();
}

