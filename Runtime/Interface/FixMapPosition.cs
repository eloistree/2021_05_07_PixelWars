﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FixMapPosition : IMapLinkedPosition
{

    public IMapDimension m_mapDimention;
    public double m_l2rPourcent;
    public double m_b2tPourcent;
    public uint m_l2rIndex;
    public uint m_t2bIndex;
    public uint m_1Dindex;

    public FixMapPosition(IMapDimension mapDimention)
    {
        m_mapDimention = mapDimention;
        m_l2rPourcent = 0;
        m_b2tPourcent = 0;
        m_l2rIndex=0;
        m_t2bIndex = 0;
    }

    public FixMapPosition(IMapDimension mapDimention, double l2rPourcent, double b2tPourcent)
    {
        SetWith(mapDimention, l2rPourcent, b2tPourcent);

    }
    public FixMapPosition(IMapDimension mapDimention, uint l2rIndex, uint t2bIndex)
    {
        SetWith(mapDimention, l2rIndex, t2bIndex);

    }
    public FixMapPosition(IMapDimension mapDimention, uint uniDimIndex)
    {
        SetWith(mapDimention, uniDimIndex);

    }

    public void SetWith(IMapDimension mapDimention, double l2rPourcent, double b2tPourcent)
    {
        if (l2rPourcent < 0.0)
            l2rPourcent = 0;
        if (l2rPourcent > 1.0)
            l2rPourcent = 1.0;
        if (b2tPourcent < 0.0)
            b2tPourcent = 0;
        if (b2tPourcent > 1.0)
            b2tPourcent = 1.0;

        m_mapDimention = mapDimention;
        m_l2rPourcent = l2rPourcent;
        m_b2tPourcent = b2tPourcent;
        m_l2rIndex = (uint)(m_mapDimention.GetWidth() * (m_l2rPourcent-0.000000001));
        m_t2bIndex = (uint)(m_mapDimention.GetHeight() * ((1.0 - m_b2tPourcent) - 0.000000001));
        m_1Dindex = m_t2bIndex * m_mapDimention.GetWidth() + m_l2rIndex;
    }

    public void SetWith(IMapDimension mapDimention, uint l2rIndex, uint t2bIndex)
    {
        m_mapDimention = mapDimention;
        if (l2rIndex < 0.0)
            l2rIndex = 0;
        if (t2bIndex < 0.0)
            t2bIndex = 0;
        if (l2rIndex >= mapDimention.GetWidth())
            l2rIndex = mapDimention.GetWidth()-1;
        if (t2bIndex >= mapDimention.GetHeight())
            t2bIndex = mapDimention.GetHeight()-1;
        m_l2rIndex = l2rIndex;
        m_t2bIndex = t2bIndex;
        m_1Dindex =( t2bIndex * mapDimention.GetWidth() ) + l2rIndex;
        m_l2rPourcent = m_l2rIndex / (double) (mapDimention.GetWidth()-1.0);
        m_b2tPourcent = 1.0- (m_t2bIndex / (double) (mapDimention.GetHeight()-1.0));
    }
    public void SetWith(IMapDimension mapDimention, uint uniDimIndex)
    {
        m_mapDimention = mapDimention;
        if (uniDimIndex >= mapDimention.GetCount())
            m_1Dindex = mapDimention.GetCount() - 1;
        else m_1Dindex = uniDimIndex;

        m_t2bIndex = m_1Dindex / mapDimention.GetWidth();
        m_l2rIndex = m_1Dindex - m_t2bIndex*mapDimention.GetWidth();
        m_l2rPourcent = m_l2rIndex / (double)(mapDimention.GetWidth() - 1.0);
        m_b2tPourcent = 1.0 - (m_t2bIndex / (double)(mapDimention.GetHeight() - 1.0));
    }

    public void GetB2T(out float bot2TopPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void GetB2T(out double bot2TopPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void GetB2T(out uint bot2TopPx)
    {
        throw new System.NotImplementedException();
    }

    public void GetB2T(out int bot2TopPx)
    {
        throw new System.NotImplementedException();
    }

    public void GetIndex(out uint index)
    {
        throw new System.NotImplementedException();
    }

    public void GetIndex(out int index)
    {
        throw new System.NotImplementedException();
    }

    public void GetL2R(out float left2RightPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void GetL2R(out double left2RightPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void GetL2R(out uint left2RightPx)
    {
        throw new System.NotImplementedException();
    }

    public void GetL2R(out int left2RightPx)
    {
        throw new System.NotImplementedException();
    }

    public void GetLatitude(out double latitude)
    {
        throw new System.NotImplementedException();
    }

    public void GetLatitude(out float latitude)
    {
        throw new System.NotImplementedException();
    }

    public void GetLatitudeAndLongitude(out double latitude, out double longitude)
    {
        throw new System.NotImplementedException();
    }

    public void GetLatitudeAndLongitude(out float latitude, out float longitude)
    {
        throw new System.NotImplementedException();
    }

    public void GetLenght(out uint index)
    {
        throw new System.NotImplementedException();
    }

    public void GetLenght(out int index)
    {
        throw new System.NotImplementedException();
    }

    public IMapDimension GetLinkedMapDimensionRef()
    {
        throw new System.NotImplementedException();
    }

    public void GetLongitude(out double longitude)
    {
        throw new System.NotImplementedException();
    }

    public void GetLongitude(out float longitude)
    {
        throw new System.NotImplementedException();
    }

    public void GetR2L(out float right2LeftPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void GetR2L(out double right2LeftPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void GetR2L(out uint right2LeftPx)
    {
        throw new System.NotImplementedException();
    }

    public void GetR2L(out int right2LeftPx)
    {
        throw new System.NotImplementedException();
    }

    public void GetT2B(out float top2BotPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void GetT2B(out double top2BotPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void GetT2B(out uint top2BotPx)
    {
        throw new System.NotImplementedException();
    }

    public void GetT2B(out int top2BotPx)
    {
        throw new System.NotImplementedException();
    }

  

    public void SetB2T(double bot2TopPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void SetB2T(uint bot2TopPx)
    {
        throw new System.NotImplementedException();
    }

    public void SetB2T(int bot2TopPx)
    {
        throw new System.NotImplementedException();
    }

    public void SetL2R(double left2RightPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void SetL2R(uint left2RightPx)
    {
        throw new System.NotImplementedException();
    }

    public void SetL2R(int left2RightPx)
    {
        throw new System.NotImplementedException();
    }

    public void SetLatitude(out double latitude)
    {
        throw new System.NotImplementedException();
    }

    public void SetLongitude(out double longitude)
    {
        throw new System.NotImplementedException();
    }

    public void SetR2L(double left2RightPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void SetT2B(double bot2TopPourcent)
    {
        throw new System.NotImplementedException();
    }

    public void Set2DTextureXY(out uint x, out uint y)
    {
        throw new System.NotImplementedException();
    }

    public void Get2DTextureXY(out uint x, out uint y)
    {
        x = m_l2rIndex;
        y = (m_mapDimention.GetHeight() - 1 - m_t2bIndex);
    }

    public void Set2DTextureXY(out int x, out int y)
    {
        throw new System.NotImplementedException();
    }
    public void Get2DTextureXY(out int x, out int y)
    {
        Get2DTextureXY(out uint ux, out uint uy);
        x = (int)ux;
        y = (int)uy;
    }

}