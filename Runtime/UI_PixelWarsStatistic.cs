﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_PixelWarsStatistic : MonoBehaviour
{

    public ComputeShaderGetterPixelWars m_computeGetter;

    public Text [] m_topTeamColor;



    public TeamsStateInMap m_teams;
    internal void Refresh()
    {
        m_teams = m_computeGetter.m_teamsStateForUI;
        m_teams. BuildReadableData();

       TeamsStateInMap.TeamInfo []top =  m_teams.GetBestBasedOnClaimed();
        for (int i = 0; i < m_topTeamColor.Length; i++)
        {
            if (i < top.Length) {
             //   m_topTeamColor[i].text = string.Format("{0:000000000}: {3:0.00}%  \t T{1:00000} D{2:00000}", top[i].m_team.GetColorId(), top[i].m_troup, top[i].m_defense, top[i].m_pourcentOwned);
                m_topTeamColor[i].text = string.Format("{0:000000000}: {1:00.00}%  ", top[i].m_team.GetColorId(), top[i].m_pourcentOwned*100f);
                m_topTeamColor[i].color = top[i].m_team.GetColor();
            }
            else m_topTeamColor[i].text = "-";
        }
    }
}
