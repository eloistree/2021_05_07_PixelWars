﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DirtyPlayerBaseCodeTestAround : MonoBehaviour
{

    public string m_lastValide;
    
    // exactly at where the user asked
    public MoveCursor m_cursorMoveListener;
    public MoveCursor m_cursorMoveToValidePixelListener;
    public delegate void MoveCursor(string playerNameId, float l2rPct, float b2tPCt);


    public HightligtCursor m_hightLightCursor;
    public delegate void HightligtCursor(string playerNameId);


    public SetColorOfCursor m_cursorColorListener;
    public delegate void SetColorOfCursor(string playerNameId, TeamColor color);
    public CreatePlayer m_createPlayer;
    public delegate void CreatePlayer(string playerNameId, TeamColor color);


    public AirdropAtCursor m_aidDropAtcursorListerner;
    public delegate void AirdropAtCursor(string playerNameId, uint troupToSend);

    public PlayerTargetAction m_fullAirDropAtcursorListerner;
    public delegate void PlayerTargetAction(string playerNameId);


    public NukeAtCursor m_nukeAtCursorListener;
    public delegate void NukeAtCursor(string playerNameId, NukeType nuke);

    public NukeAtPosition m_nukeAtPositionListener;
    public delegate void NukeAtPosition(string playerNameId, NukeType nuke, PourcentToPixelCoordinate where);

    public LaserStrikeAtCursor m_laserStrikeListener;
    public delegate void LaserStrikeAtCursor(string playerNameId, PourcentToPixelCoordinate where);


    public MessageInChat m_chatMessageListener;
    public delegate void MessageInChat(string playerNameId,string message, uint troupCostProposed);




    public Queue <string> m_inWaiting = new Queue<string>();

    public void DoActionAsPlayer(string actionAsText) {

       m_inWaiting.Enqueue(actionAsText);


    }

    public void Update()
    {
        TryToFlushInWaitingRequest();
    }

    private void TryToFlushInWaitingRequest()
    {
        if (m_inWaiting.Count <= 0) 
            return;
        string tosend;
        
        while (m_inWaiting.Count > 0){ 
            tosend = m_inWaiting.Dequeue();
            ConvertTextToAction(tosend);
        }
    }

    private void ConvertTextToAction(string text)
    {

        string[] tokens = text.Split(':');
        if (tokens.Length == 2)
        {
            string player = tokens[0].Trim().ToLower();
            string cmd = tokens[1].Trim().ToLower();
            if (cmd.IndexOf("cursor") == 0 && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 3)
                {

                    if (float.TryParse(cursorToken[1], out float l2r))
                    {
                        if (float.TryParse(cursorToken[2], out float b2t))
                        {
                            if (l2r > 1.0) l2r /= 100;
                            if (b2t > 1.0) b2t /= 100;
                            m_lastValide = text;
                            m_cursorMoveListener(player, l2r, b2t);
                        }
                    }
                }



            }
            if (cmd.IndexOf("color") == 0 && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 2)
                {

                    if (uint.TryParse(cursorToken[1].Trim(), out uint id))
                    {

                        m_lastValide = text;
                        m_cursorColorListener(player, TeamColor.GetFromUint(id));
                    }
                }
                if (cursorToken.Length == 4)
                {

                    if (uint.TryParse(cursorToken[1], out uint r))
                    {
                        if (uint.TryParse(cursorToken[2], out uint g))
                        {
                            if (uint.TryParse(cursorToken[3], out uint b))
                            {
                                m_lastValide = text;
                                m_cursorColorListener(player, TeamColor.GetIdFrom(r, g, b));
                            }
                        }
                    }
                }
            }
            if (cmd.IndexOf("hello") == 0 && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 1)
                {
                    m_lastValide = text;
                    m_createPlayer(player, TeamColor.GetRandom());

                }

                if (cursorToken.Length == 2)
                {

                    if (uint.TryParse(cursorToken[1].Trim(), out uint id))
                    {

                        m_lastValide = text;

                        m_createPlayer(player, TeamColor.GetFromUint(id));
                    }
                }
                if (cursorToken.Length == 4)
                {

                    if (uint.TryParse(cursorToken[1], out uint r))
                    {
                        if (uint.TryParse(cursorToken[2], out uint g))
                        {
                            if (uint.TryParse(cursorToken[3], out uint b))
                            {
                                m_lastValide = text;

                                m_createPlayer(player, TeamColor.GetIdFrom(r, g, b));
                            }
                        }
                    }
                }
            }
            if (cmd.IndexOf("fullairdrop ") == 0 && m_cursorMoveListener != null)
            {
                m_lastValide = text;
                m_fullAirDropAtcursorListerner(player);

            }
            if (cmd.IndexOf("airdrop ") == 0 && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 2)
                {

                    m_lastValide = text;
                    uint.TryParse(cursorToken[1], out uint troup);
                    m_aidDropAtcursorListerner(player, troup);

                }
            }
        }
    }

    private string RemoveSpaces(string cmd)
    {
        while (cmd.IndexOf("  ") >= 0)
            cmd = cmd.Replace("  ", " ");
        return cmd;
    }
}
