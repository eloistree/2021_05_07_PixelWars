﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelWarsPlayerMetaInformationRegisterMono : MonoBehaviour
{
    public List<UnityPlayerMetaInformation> m_playersMetaInfoInGame;


    public void AddMoneyToAll(int  value) {

        foreach (var item in m_playersMetaInfoInGame)
        {
            item.m_wallet.Add((uint)value);
        }
    }

    public UnityPlayerMetaInformation [] GetAllPlayersInfo()
    {
        return m_playersMetaInfoInGame.ToArray();
    }

    public void GetWallet(PlayerNameId playerName, out bool foundWallet, out TroupWallet wallet)
    {
        HasMeta(playerName, out bool found, out UnityPlayerMetaInformation player);

        if (found)
        {
            foundWallet = true;
           wallet = player.m_wallet;
        }
        else {
            foundWallet = false;
            wallet = null;
        }
    }
    public void RemoveAll(PlayerNameId playerId) {
        for (int i = m_playersMetaInfoInGame.Count-1; i >= 0; i--)
        {
            if (m_playersMetaInfoInGame[i].m_playerId.m_name == playerId.m_name || m_playersMetaInfoInGame[i].m_playerId.m_inGameId == playerId.m_inGameId)
                m_playersMetaInfoInGame.RemoveAt(i);
        }
    }

    public void CreateOrOverridMetaFor(PlayerNameId playerId)
    {
        RemoveAll(playerId);
       
        UnityPlayerMetaInformation newCursor = new UnityPlayerMetaInformation(playerId);
        m_playersMetaInfoInGame.Add(newCursor);
    }

    public void CreateMetaFor(PlayerNameId playerId)
    {

        HasMeta(playerId, out bool found, out UnityPlayerMetaInformation player);
        if (found) return;

        UnityPlayerMetaInformation newCursor = new UnityPlayerMetaInformation(playerId);
        m_playersMetaInfoInGame.Add(newCursor);
    }

    public void HasMeta(PlayerNameId playerName, out bool found, out UnityPlayerMetaInformation playerMeta)
    {
        for (int i = 0; i < m_playersMetaInfoInGame.Count; i++)
        {

            if (m_playersMetaInfoInGame[i].m_playerId == playerName)
            {
                found = true;
                playerMeta = m_playersMetaInfoInGame[i];
                return;
            }
        }

        found = false;
        playerMeta = null;
    }

}

[System.Serializable]
public class UnityPlayerMetaInformation
{

    public PlayerNameId m_playerId;
    public TroupWallet m_wallet;

    public UnityPlayerMetaInformation(PlayerNameId playerId)
    {
        m_playerId = playerId;
        m_wallet = new TroupWallet();
    }
   
}
