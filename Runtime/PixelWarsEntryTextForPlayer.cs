﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PixelWarsEntryTextForPlayer : MonoBehaviour
{

    public string m_lastValide;
    public Text m_lastValideDebug;
    public StringEvent m_lastValideEvent;
    [System.Serializable]
    public class StringEvent : UnityEvent<string> { };
    // exactly at where the user asked
    public MoveCursor m_cursorMoveListener;
    public MoveCursor m_cursorMoveToValidePixelListener;
    public delegate void MoveCursor(string playerNameId, float l2rPct, float b2tPCt);


    public TranslateCursor m_cursorTranslate;
    public delegate void TranslateCursor(string playerNameId, float l2rPct, float b2tPCt);


    public HightligtCursor m_hightLightCursor;
    public delegate void HightligtCursor(string playerNameId);


    public SetColorOfCursor m_cursorColorListener;
    public delegate void SetColorOfCursor(string playerNameId, TeamColor color);
    public CreatePlayer m_createPlayer;
    public delegate void CreatePlayer(string playerNameId, TeamColor color);


    public AirdropAtCursor m_aidDropAtcursorListerner;
    public delegate void AirdropAtCursor(string playerNameId, uint troupToSend);

    public DefenseAtCursor m_defenseAtcursorListerner;
    public delegate void DefenseAtCursor(string playerNameId, uint troupToSend, int radiusInPixel);

    public PlayerTargetAction m_fullAirDropAtcursorListerner;
    public delegate void PlayerTargetAction(string playerNameId);


    public NukeAtCursor m_nukeAtCursorListener;
    public delegate void NukeAtCursor(string playerNameId, NukeType nuke);

    public NukeAtPosition m_nukeAtPositionListener;
    public delegate void NukeAtPosition(string playerNameId, NukeType nuke, PourcentToPixelCoordinate where);

    public LaserStrike m_laserStrikeListener;
    public delegate void LaserStrike(string playerNameId, PourcentToPixelCoordinate where);
    public LaserStrikeAtCursor m_laserStrikeListenerAtCursor;
    public delegate void LaserStrikeAtCursor(string playerNameId);


    public MessageInChat m_chatMessageListener;
    public delegate void MessageInChat(string playerNameId, string message, uint troupCostProposed);

    public WorldCameraFocus m_cameraFocus;
    public delegate void WorldCameraFocus(string playerNameId, float latitude, float longitude, float focusInPourcent);


    public LocationTextToTextureLocation m_locationsCoordinate;


    public PauseWorldRotationForFocus m_worldRotate;

    public Queue <string> m_inWaiting = new Queue<string>();

    public void DoActionAsPlayer(string actionAsText) {

       m_inWaiting.Enqueue(actionAsText);


    }

    public void Update()
    {
        TryToFlushInWaitingRequest();
        TryToFlushInDelay();
    }

    private void TryToFlushInDelay()
    {
        float t = Time.deltaTime;
        for (int i = m_delay.Count-1; i >=0 ; i--)
        {
            m_delay[i].m_timeLeft -= t;
            if (m_delay[i].m_timeLeft <= 0) {
                DoActionAsPlayer(m_delay[i].m_cmd);
                m_delay.RemoveAt(i);
            } 
        }
    }

    private void TryToFlushInWaitingRequest()
    {
        if (m_inWaiting.Count <= 0) 
            return;
        string tosend;
        
        while (m_inWaiting.Count > 0){ 
            tosend = m_inWaiting.Dequeue();
            ConvertTextToAction(tosend);
        }
    }

    public class DelayCmd {
        public float m_timeLeft;
        public string m_cmd;

        public DelayCmd(float timeLeft, string cmd)
        {
            m_timeLeft = timeLeft;
            m_cmd = cmd;
        }

    }

    public List<string> m_historyDebug = new List<string>();
    public void AddHistory(string msg) {
        m_historyDebug.Add(msg);
        while (m_historyDebug.Count > 10)
            m_historyDebug.RemoveAt(0);
    }
    public List<DelayCmd> m_delay = new List<DelayCmd>();
    private void ConvertTextToAction(string text)
    {
        AddHistory(text);

        if (text.IndexOf(">") > -1)
        {
            int playerSpliter = text.IndexOf('Ŧ');
            if (playerSpliter > 0) { 
                

                string player = text.Substring(0, playerSpliter).Trim().ToLower();
                string cmd = text.Substring(playerSpliter+1 ).Trim().ToLower();

                //hello 25500000 > cursor 0.5 0.5 >5000 >fullairdrop>5000>nuke h
                string[] cmds = cmd.Split('>');
                uint delayCount = 0;
                for (int i = 0; i < cmds.Length; i++)
                {
                    if (uint.TryParse(cmds[i], out uint timeInMs))
                    {
                        delayCount += timeInMs;
                    }
                    else
                    {
                        delayCount += 100;
                        m_delay.Add(new DelayCmd(delayCount / 1000f, player+ "Ŧ" + cmds[i]));
                    }
                }


                return;
            }
        }


        string[] tokens = text.Split('Ŧ');
        if (tokens.Length == 2)
        {
            string player = tokens[0].Trim().ToLower();
            string cmd = tokens[1].Trim().ToLower();

            if (cmd.IndexOf("camera ") == 0) {

                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 4)
                {

                    if (float.TryParse(cursorToken[1], out float l2r))
                    {
                        if (float.TryParse(cursorToken[2], out float b2t))
                        {
                            if (float.TryParse(cursorToken[3], out float camFocus))
                            {
                                if (camFocus > 1.0) camFocus /= 100;

                                ConvertLRBT2LatLong(l2r, b2t, out float latitude, out float longitude);
                                SetLastMessageValide(text);
                                m_cameraFocus(player, latitude, longitude, Mathf.Clamp(camFocus, 0f, 1f));
                            }

                        }
                    }
                }
                if (cursorToken.Length == 3)
                {

                    if (float.TryParse(cursorToken[1], out float l2r))
                    {
                        if (float.TryParse(cursorToken[2], out float b2t))
                        {
                            SetLastMessageValide(text);
                            ConvertLRBT2LatLong(l2r, b2t, out float latitude, out float longitude);
                            m_cameraFocus(player, latitude, longitude, .5f);
                        }
                    }
                    else
                    {
                        if (float.TryParse(cursorToken[2], out float camFocus))
                        {
                            m_locationsCoordinate.TryToFound(cursorToken[1].Trim().ToLower(), out bool found, out float l2rr, out float b2tt);
                            if (found)
                            {

                                ConvertLRBT2LatLong(l2rr, b2tt, out float latitude, out float longitude);
                                SetLastMessageValide(text);
                                m_cameraFocus(player,latitude, longitude, camFocus);
                            }
                        }


                    }
                }
                if (cursorToken.Length == 2)
                {

                    m_locationsCoordinate.TryToFound(cursorToken[1].Trim().ToLower(), out bool found, out float l2r, out float b2t);
                    if (found)
                    {

                        SetLastMessageValide(text);
                        m_cameraFocus(player, (l2r - 0.5f)*2f * 180f, (b2t - 0.5f) * 2f * 90f, 0.5f);
                    }
                }
            }

            if (cmd.IndexOf("cursor move ") == 0 && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 4)
                {

                    if (float.TryParse(cursorToken[2], out float l2r))
                    {
                        if (float.TryParse(cursorToken[3], out float b2t))
                        {
                            if (l2r > 1.0) l2r /= 100;
                            if (b2t > 1.0) b2t /= 100;
                            SetLastMessageValide(text);
                            m_cursorTranslate(player, l2r, b2t);
                        }
                    }
                }
            }

            else if (cmd.IndexOf("cursor") == 0 && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 3)
                {

                    if (float.TryParse(cursorToken[1], out float l2r))
                    {
                        if (float.TryParse(cursorToken[2], out float b2t))
                        {
                            if (l2r > 1.0) l2r /= 100;
                            if (b2t > 1.0) b2t /= 100;
                            SetLastMessageValide(text);
                            m_cursorMoveListener(player, l2r, b2t);
                        }
                    }
                }
                if (cursorToken.Length == 2) {
                    m_locationsCoordinate.TryToFound(cursorToken[1].Trim().ToLower(), out bool found, out float l2r, out float b2t);
                    if (found) {
                        m_cursorMoveListener(player, l2r, b2t);
                    }

                }
            }
            if (cmd.IndexOf("color") == 0 && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 2)
                {

                    if (uint.TryParse(cursorToken[1].Trim(), out uint id))
                    {

                        SetLastMessageValide(text);
                        m_cursorColorListener(player, TeamColor.GetFromUint(id));
                    }
                }
                if (cursorToken.Length == 4)
                {

                    if (uint.TryParse(cursorToken[1], out uint r))
                    {
                        if (uint.TryParse(cursorToken[2], out uint g))
                        {
                            if (uint.TryParse(cursorToken[3], out uint b))
                            {
                                SetLastMessageValide(text);
                                m_cursorColorListener(player, TeamColor.GetIdFrom(r, g, b));
                            }
                        }
                    }
                }
            }

            if (cmd.Length==7 && cmd[0]=='#') {
                //#ff0000
                ConvertColorHexaToColor(cmd.Substring(1),out bool converted, out int r, out int g, out int b);
                if (converted) {
                    TeamColor c = TeamColor.CreateFrom(r, g, b);
                    m_createPlayer(player, c);
                    m_cursorColorListener(player, c);
                } 

            }
            if ((cmd.IndexOf("hello") == 0  || cmd.IndexOf("join") == 0 ) && m_cursorMoveListener != null)
            {


                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 1)
                {
                    SetLastMessageValide(text);
                    m_createPlayer(player, TeamColor.GetRandom());

                }

                if (cursorToken.Length == 2)
                {

                    if (uint.TryParse(cursorToken[1].Trim(), out uint id))
                    {

                        SetLastMessageValide(text);

                        m_createPlayer(player, TeamColor.GetFromUint(id));
                    }
                }
                if (cursorToken.Length == 4)
                {

                    if (uint.TryParse(cursorToken[1], out uint r))
                    {
                        if (uint.TryParse(cursorToken[2], out uint g))
                        {
                            if (uint.TryParse(cursorToken[3], out uint b))
                            {
                                SetLastMessageValide(text);
                                m_createPlayer(player, TeamColor.GetIdFrom(r, g, b));
                            }
                        }
                    }
                }
            }
            if ((cmd.IndexOf("fullairdrop") == 0 || cmd == "fa") && m_cursorMoveListener != null)
            {
                SetLastMessageValide(text);
                m_fullAirDropAtcursorListerner(player);

            }
            if ((cmd.IndexOf("camera rotation on") == 0) && m_cursorMoveListener != null)
            {

                SetLastMessageValide(text);
                m_worldRotate.m_autoRun = true;

            }
            if ((cmd.IndexOf("camera rotation off") == 0) && m_cursorMoveListener != null)
            {
                SetLastMessageValide(text);
                m_worldRotate.m_autoRun = false;
            }
            if ((cmd.IndexOf("nuke ") == 0|| cmd.IndexOf("n ") == 0) && m_cursorMoveListener != null)
            {
                cmd = cmd.ToLower();
                SetLastMessageValide(text);
                NukeType nuke = NukeType.Atomic;
                if (cmd.IndexOf("h") > 0)
                    nuke = NukeType.H;
                else if (cmd.IndexOf("mini") > 0)
                    nuke = NukeType.Mini;
                else if (cmd.IndexOf("atomic") > 0)
                    nuke = NukeType.Atomic;
                else if (cmd.IndexOf("air") > 0)
                    nuke = NukeType.Airstrike;
                m_nukeAtCursorListener(player,nuke);


            }
            if ((cmd.IndexOf("airdrop ") == 0 || cmd.IndexOf("a ") == 0) && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 2)
                {

                    SetLastMessageValide(text);
                    uint.TryParse(cursorToken[1], out uint troup);
                    m_aidDropAtcursorListerner(player, troup);

                }
                if (cursorToken.Length == 4)
                {

                    SetLastMessageValide(text);
                    uint.TryParse(cursorToken[3], out uint troup);
                    float.TryParse(cursorToken[1], out float x);
                    float.TryParse(cursorToken[2], out float y);

                    m_cursorMoveListener(player, x, y);
                    m_aidDropAtcursorListerner(player, troup);

                }
            }
            if ((cmd.IndexOf("defense ") == 0 || cmd.IndexOf("d ") == 0) && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 2)
                {

                    SetLastMessageValide(text);
                    uint.TryParse(cursorToken[1], out uint troup);
                    m_defenseAtcursorListerner(player, troup, 0);

                }
                if (cursorToken.Length == 3)
                {

                    SetLastMessageValide(text);
                    uint.TryParse(cursorToken[1], out uint troup);
                    int.TryParse(cursorToken[2], out int pixel);
                    m_defenseAtcursorListerner(player, troup, pixel);

                }
            
            }
            if (cmd.IndexOf("laserstrike") == 0 && m_cursorMoveListener != null)
            {
                string[] cursorToken = RemoveSpaces(cmd).Split(' ');
                if (cursorToken.Length == 1)
                {

                    SetLastMessageValide(text);
                    uint.TryParse(cursorToken[1], out uint troup);
                    m_laserStrikeListenerAtCursor(player);

                }
                if (cursorToken.Length == 3)
                {

                    SetLastMessageValide(text);

                    float.TryParse(cursorToken[1], out float xpct);
                    float.TryParse(cursorToken[2], out float ypct);
                    m_laserStrikeListener(player, new PourcentToPixelCoordinate(xpct, ypct));

                }
            }

            if (cmd.IndexOf("cheat little money") == 0 && m_cursorMoveListener != null)
            {
             
               // m_
            }
            //if (cmd.IndexOf("defense ") == 0 && m_cursorMoveListener != null)
            //{
            //    string[] cursorToken = RemoveSpaces(cmd).Split(' ');
            //    if (cursorToken.Length == 3)
            //    {

            //        m_lastValide = text;
            //        float.TryParse(cursorToken[1], out float spreadAround);
            //        uint.TryParse(cursorToken[1], out uint troup);
            //        m_laserStrikeListener(player, );

            //    }

            //}


        }
    }

    private void ConvertLRBT2LatLong(float l2r, float b2t, out float latitude, out float longitude)
    {
        latitude = (l2r - 0.5f) * 2f * 180f;
        longitude = (b2t - 0.5f) * 2f * 90f;
    }

    private void ConvertColorHexaToColor(string hexaValue, out bool converted, out int r, out int g, out int b)
    {
        r = g = b = 0;
        converted = false;
        if(hexaValue.Length!=6)
            return;
        try
        {
            r = Convert.ToInt32(hexaValue.Substring(0, 2), 16);
            g = Convert.ToInt32(hexaValue.Substring(2, 2), 16);
            b = Convert.ToInt32(hexaValue.Substring(4, 2), 16);
        }
        catch (Exception) { return; }
        converted = true;
    }

    private void SetLastMessageValide(string text)
    {
        m_lastValide = text;
        if (m_lastValideDebug!=null)
            m_lastValideDebug.text = text;

        m_lastValideEvent.Invoke(m_lastValide);
    }

    private string RemoveSpaces(string cmd)
    {
        while (cmd.IndexOf("  ") >= 0)
            cmd = cmd.Replace("  ", " ");
        return cmd;
    }
}
