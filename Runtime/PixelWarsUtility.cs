﻿using System;

public class PixelWarsUtility
{
    public static void GetTextureXY(V2ReadingDirection l2RB2T, int width, int height, float l2rPct, float b2tPct, out int l2rIndex, out int t2bIndex)
    {
        if (l2RB2T == V2ReadingDirection.L2RB2T) {
            b2tPct = 1f - b2tPct;
        }
        l2rIndex=(int)( l2rPct*width);
        t2bIndex = (int)( b2tPct * height);
    }

    internal static void GetTextureIndex(V2ReadingDirection l2RB2T, int width, float l2rPct, float b2tPct, out int index)
    {
        if (l2RB2T == V2ReadingDirection.L2RB2T)
        {
            b2tPct = 1f - b2tPct;
        }
        index = (int)(l2rPct + (b2tPct * width));
    }
}
public enum V2ReadingDirection { L2RB2T, L2RT2B, Undefined}