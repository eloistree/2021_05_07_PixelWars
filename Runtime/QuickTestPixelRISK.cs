﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickTestPixelRISK : MonoBehaviour
{
    [Header("Given")]
    public int m_width = 800;
    public int m_height = 450;

    public int m_pixelCount = 450;
    public int m_whiteCount = 450;
    public Texture2D m_blackAndWhiteMapClaimable;
    public Texture2D m_backgroundPhotoOfTheMap;

    [Header("Current Game")]
    public RenderTexture m_claimedColorMap;
    public RenderTexture m_populationColorMap;


    public void AddUnity(Vector2 pourcentBotLeftToTopRight, int soldiers, Color color)
    {
        AddUnity(pourcentBotLeftToTopRight.x, pourcentBotLeftToTopRight.y, soldiers, color);
    }

        public void AddUnity(float left2Right, float bot2top, int soldiers, Color color)
        {

            int colorId = (int)((color.r * 255f) * 1000000f + (color.g * 255f) * 1000f + (color.b * 255f));
      //  Debug.Log("ColorID:" + colorId);
        GetPixelXY(left2Right, bot2top, out int index);
        m_unityPerPixel[index] += soldiers;
        m_pixelsColorClaim[index] = Color.red;
        m_teamPerPixel[index] = colorId;

       // GetPixelXY(left2Right, bot2top, out int x, out int y);

        //for (int j = -5; j < 5; j++)
        //{
        //    for (int d = -5; d < 5; d++)
        //    {
        //        m_unityPerPixel[GetIndex(x+j, y+d)] += soldiers;
        //        m_pixelsColorClaim[GetIndex(x + j, y + d)] = color;
        //        m_teamPerPixel[GetIndex(x + j, y + d)] = colorId;

        //    }
        //}
        



    }

    public int GetIndex(int x, int y) {
        return (y * m_width) + (x); 
    }
    public void GetPixelXY(float left2Right, float top2Down, out int index)
    {
        GetPixelXY(left2Right, top2Down, out int x, out int y);
      
        index =(y * m_width) + (x );
    }
    public void GetPixelXY(float left2Right, float top2Down, out int left2RightPixel, out int bot2TopPixel) {

        left2RightPixel =(int)( left2Right * (float)m_width);
        bot2TopPixel = (int) (top2Down * (float)m_height);
    }



    private Color[] m_territory;
    private bool[] m_territoryClaimableAsBoolean;
    private int[] m_territoryClaimableAsInteger;

    private int[] m_unityPerPixel;
    private int[] m_teamPerPixel;
    private Color [] m_pixelsColorClaim;


    public Color m_noUnityColor= Color.white;
    public ulong m_lotOfUnityNumber=1000;
    public Color m_lotOfUnityColor = Color.green;



    [Header("Debug")]
    public RawImage m_mapColorDebug;
    public RawImage m_mapPopulationDebug;

    void Awake()
    {
        m_width = m_blackAndWhiteMapClaimable.width;
        m_height = m_blackAndWhiteMapClaimable.height;

        m_territory = m_blackAndWhiteMapClaimable.GetPixels();
        m_territoryClaimableAsBoolean = new bool[m_territory.Length];
        m_territoryClaimableAsInteger = new int[m_territory.Length];
        m_randomDirection = new int[m_territory.Length];
        m_teamPerPixel = new int[m_territory.Length];

        for (int i = 0; i < m_territory.Length; i++)
        {
            if (m_territory[i].r > 0.99 && m_territory[i].g > 0.99 && m_territory[i].b > 0.99)
            {
                m_whiteCount++;
                m_territoryClaimableAsBoolean[i] = true;
                m_territoryClaimableAsInteger[i] = 1;
            }
            else {
                m_territoryClaimableAsBoolean[i] = false;
                m_territoryClaimableAsInteger[i] = 0;
            }
        }
        m_pixelCount = m_territory.Length;
 
        m_unityPerPixel = new int[m_pixelCount];
        m_pixelsColorClaim = new Color[m_pixelCount];

        m_claimedColorMap = new  RenderTexture(m_width, m_height,0, RenderTextureFormat.ARGB32);
       m_claimedColorMap.enableRandomWrite = true;
        m_claimedColorMap.Create();

        m_populationColorMap = new RenderTexture(m_width, m_height,0, RenderTextureFormat.ARGB32);
       m_populationColorMap.enableRandomWrite = true;
        m_populationColorMap.Create();

        m_mapColorDebug.texture = m_claimedColorMap;
        m_mapPopulationDebug.texture = m_populationColorMap;


        m_drawingPopulation = new Color[m_pixelCount];
        m_drawingClaimed = new Color[m_pixelCount];

//        RefreshMapWithColor();
        InvokeRepeating("RefreshComputeShader", 0.1f, m_timeBetweenRound);
    }
    public float m_timeBetweenRound=0.5f;

    private Color[] m_drawingPopulation;
    private Color[] m_drawingClaimed;

    private bool IsPixelClaimable(int i)
    {
        
        return m_territoryClaimableAsBoolean[i];
    }

    public void RefreshMapWithColor() {



        
        for (int i = 0; i < m_pixelCount; i++)
        {
            if (IsPixelClaimable(i))
            {
                m_drawingPopulation[i] = Color.Lerp(m_noUnityColor, m_lotOfUnityColor, m_unityPerPixel[i] / (float)m_lotOfUnityNumber);
                m_drawingClaimed[i] = m_pixelsColorClaim[i];
            }
           
        }


        for (int i = 0; i < m_pixelCount; i++)
        {
            if (IsPixelClaimable(i))
            {
                m_drawingPopulation[i] = Color.Lerp(m_noUnityColor, m_lotOfUnityColor, m_unityPerPixel[i] / (float)m_lotOfUnityNumber);
                m_drawingClaimed[i] = m_pixelsColorClaim[i];
            }
            else {
                m_drawingPopulation[i] = Color.black;
                m_drawingClaimed[i] = Color.black; 
            }
        }

    }

    private void AddSolderAt(int x, int y, int unity)
    {
        if (x >= 0 && x < m_width && y >= 0 && y < m_height)
        { 
            m_unityPerPixel[GetIndex(x, y )] += unity;
        }
    }




    public ComputeShader m_manageSoldierOnMap;
    public ComputeShader m_managePropagationOnMap;

    int count=0;
    public void RefreshComputeShader()
    {
        count++;
        DoPropagation();

        int kernel = m_manageSoldierOnMap.FindKernel("CSMain");


        m_manageSoldierOnMap.SetInt("m_width", m_width);
        m_manageSoldierOnMap.SetInt("m_height", m_height);
        m_manageSoldierOnMap.SetInt("m_pixelCount", m_pixelCount);
        m_manageSoldierOnMap.SetInt("rng_state",(int) UnityEngine.Random.value*10000);

        var bufferSoldier = new ComputeBuffer(m_unityPerPixel.Length, sizeof(int));
        bufferSoldier.SetData(m_unityPerPixel);
        var bufferTeam = new ComputeBuffer(m_teamPerPixel.Length, sizeof(int));
        bufferTeam.SetData(m_teamPerPixel);
        var bufferValide = new ComputeBuffer(m_territoryClaimableAsInteger.Length, sizeof(int));
        bufferValide.SetData(m_territoryClaimableAsInteger);

        m_manageSoldierOnMap.SetBuffer(kernel, "m_soldierOnMap", bufferSoldier);
        m_manageSoldierOnMap.SetBuffer(kernel, "m_valideTerritory", bufferValide);

        m_manageSoldierOnMap.SetTexture(kernel, "m_claimedMap", m_claimedColorMap);
        m_manageSoldierOnMap.SetBuffer(kernel, "m_colorTeamOnMap", bufferTeam);
        m_manageSoldierOnMap.SetTexture(kernel, "m_populationMap", m_populationColorMap);

        m_manageSoldierOnMap.Dispatch(kernel, m_width / 8, m_height / 8, 1);




        bufferSoldier.Release();
        bufferValide.Release();
        bufferTeam.Release();
    }
    public int [] m_randomDirection = new int[20];

    public int m_frameCount=0;
    public void DoPropagation()
    {
        int kernel = m_managePropagationOnMap.FindKernel("CSMain");
        m_frameCount++;

        m_managePropagationOnMap.SetInt("m_width", m_width);
        m_managePropagationOnMap.SetInt("m_height", m_height);
        m_managePropagationOnMap.SetInt("m_pixelCount", m_pixelCount);
        m_managePropagationOnMap.SetInt("m_globalPopulationUp", (m_frameCount%10==0)?1:0);


        var bufferSoldier = new ComputeBuffer(m_unityPerPixel.Length, sizeof(int));
        bufferSoldier.SetData(m_unityPerPixel);
        var bufferTeam = new ComputeBuffer(m_teamPerPixel.Length, sizeof(int));
        bufferTeam.SetData(m_teamPerPixel);
        var bufferValide = new ComputeBuffer(m_territoryClaimableAsInteger.Length, sizeof(int));
        bufferValide.SetData(m_territoryClaimableAsInteger);


        m_managePropagationOnMap.SetInt("m_randomDirectionCount", m_randomDirection.Length);
        SetRandomNess(ref m_randomDirection);
        var randomBufferValide = new ComputeBuffer(m_randomDirection.Length, sizeof(int));
        randomBufferValide.SetData(m_randomDirection);

        m_managePropagationOnMap.SetBuffer(kernel, "m_soldierOnMap", bufferSoldier);
        m_managePropagationOnMap.SetBuffer(kernel, "m_colorTeamOnMap", bufferTeam);
        m_managePropagationOnMap.SetBuffer(kernel, "m_valideTerritory", bufferValide);
        m_managePropagationOnMap.SetBuffer(kernel, "m_randomDirection", randomBufferValide);

        m_managePropagationOnMap.Dispatch(kernel, m_width / 8, m_height / 8, 1);

        bufferSoldier.GetData(m_unityPerPixel);
        bufferTeam.GetData(m_teamPerPixel);

        randomBufferValide.Release();
        bufferSoldier.Release();
        bufferValide.Release();
        bufferTeam.Release();
    }

    private void SetRandomNess(ref int[] randomDirection)
    {
        for (int i = 0; i < randomDirection.Length; i++)
        {
            randomDirection[i] =(int) (UnityEngine.Random.value *20000f )% 8;

        }
    }
}
