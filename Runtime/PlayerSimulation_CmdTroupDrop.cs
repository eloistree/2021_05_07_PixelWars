﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSimulation_CmdTroupDrop : MonoBehaviour
{

    //public QuickTestPixelRISK m_risk;
    public ComputeShaderTroupPixelWars m_PixelWar;
    public string m_playerName="";
    public Color m_team;
    public string m_debugCOlor;
    public float m_repeat=1f;
    public uint m_troupCount = 10;
    [Range(0,1)]
    public float m_left2Right;
    [Range(0, 1)]
    public float m_bot2top;
    public float m_dispection=0.1f;

    IEnumerator Start()
    {
        m_debugCOlor = TeamColor.GetIdFrom(m_team).ToString(); 
        yield return new WaitForSeconds(1);
        while (true) {

            yield return m_repeat;
            yield return new WaitForEndOfFrame();
            AddTroup();
        }
    }

    private void AddTroup()
    {
        //if (m_risk)
        //    m_risk.AddUnity(m_left2Right, m_bot2top, (int) m_troupCount, m_team);
        if (m_PixelWar)
            m_PixelWar.AddAirDrop(m_left2Right + GetDispection(), m_bot2top + GetDispection(), m_troupCount, TroupType.Moving, m_team, 0); ;
    }

    private float GetDispection()
    {
        return UnityEngine.Random.Range(-m_dispection, m_dispection);
    }
}
