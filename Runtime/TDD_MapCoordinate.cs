﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TDD_MapCoordinate : MonoBehaviour
{
    public Texture2D m_sourceMap;
    public Texture2D m_debugMap;
    public RawImage m_debugMapImage;

    [Range(0.0f,1.0f)]
    public double m_l2r, m_b2t;
    public FixMapPosition m_mapPositionFromPourcent;

    [Range(0, 512)]
    public uint m_x, m_y;
    public FixMapPosition m_mapPositionFrom2DIndex;

    [Range(0, 512*512)]
    public uint m_index;
    public FixMapPosition m_mapPositionFrom1DIndex;
    public MapDimensionDefault m_mapDimension;

    public uint m_pixelCount;

    private void OnValidate()
    {
        if (m_sourceMap == null)
            return;
        m_mapDimension = new MapDimensionDefault((uint)m_sourceMap.width, (uint)m_sourceMap.height);
        m_debugMap = new Texture2D(m_sourceMap.width, m_sourceMap.height);
        m_debugMap.SetPixels(m_sourceMap.GetPixels());



        m_mapPositionFromPourcent = new FixMapPosition(m_mapDimension, m_l2r, m_b2t);
        m_mapPositionFrom2DIndex = new FixMapPosition(m_mapDimension, m_x, m_y);
        m_mapPositionFrom1DIndex = new FixMapPosition(m_mapDimension, m_index);

        int x, y;
        m_mapPositionFromPourcent.Get2DTextureXY(out  x, out  y);
        m_debugMap.SetPixel(x, y, Color.green);
        m_mapPositionFrom2DIndex.Get2DTextureXY(out  x, out  y);
        m_debugMap.SetPixel(x, y, Color.red);
        m_mapPositionFrom1DIndex.Get2DTextureXY(out  x, out  y);
        m_debugMap.SetPixel(x, y, Color.blue);

        m_debugMap.Apply();
        if (m_debugMapImage != null)
            m_debugMapImage.texture = m_debugMap;
        m_pixelCount = (uint) m_sourceMap.GetPixels32().Length;
    }
}
