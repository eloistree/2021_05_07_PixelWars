﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public interface IPixelWarsState
{

    uint GetPixelCount();
    uint GetPixelHeight();
    uint GetPixelWidth();
    int[] GetColorClaimed();
    int[] GetMovingTroup();
    int[] GetStaticTroup();
    bool[] GetValidePixel();
}

public interface IPixelWarsStateRendering
{
    
    Texture2D GetColorClaimedMap();
    Texture2D GetPopulationMap();

    Texture2D GetDisplayMapUsed();
    Texture2D GetValidationMapUsed();

}


public interface IPixelWarsInteraction
{

    void TryToAllocateMovingTroup(uint troup, TroupType troupType, float left2rightPourcent, float bot2topPoucent);
    void TryToAllocateMovingTroupRandomInRadius(uint troup, TroupType troupType, float left2rightPourcent, float bot2topPoucent, float radiusInPourcent);
    void TryToAllocateMovingTroupRandomInRadius(uint troup, TroupType troupType, float left2rightPourcentStart, float bot2topPoucentStart, float left2rightPourcentEnd, float bot2topPoucentEnd, float radiusInPourcent);
    void TryToAllocateMovingTroupFlatInRadius(uint troup, TroupType troupType, float left2rightPourcent, float bot2topPoucent, float radiusInPourcent);
    void TryToAllocateMovingTroupFlatInRadius(uint troup, TroupType troupType, float left2rightPourcentStart, float bot2topPoucentStart, float left2rightPourcentEnd, float bot2topPoucentEnd, float radiusInPourcent);
}
public enum TroupType { Moving, Static}


public interface IImportantStatistique
{

    IGeneralTeamRank GetLeadboard();


}

public interface IGeneralTeamRank
{

  void GetTop1(out IColorTeamInformation team );
  void GetTop3(out IColorTeamInformation top1, out IColorTeamInformation top2, out IColorTeamInformation top3);
  void GetTopN(int topCount, out IColorTeamInformation [] team);

}

public interface IColorTeamInformation {

    double GetPourcentOfTheMap();
    decimal GetPixelCount();

}
