﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelWarsGameInitPreferenceMono : MonoBehaviour
{
    public PixelWarsGameInitPreference m_data;


    private void OnValidate()
    {
        if(Application.isPlaying)
        RefreshData();
    }

    private void RefreshData()
    {

    }
}

[System.Serializable]
public class PixelWarsGameInitPreference
{
    [Header("Regrow")]
    public float m_timeBetweenRegrowInSeconds = 5 * 60;
    public int m_maxRegrowMovingTroup = 20;
    public int m_movingTroupPerRegrow = 1;
    public int m_maxRegrowStaticTroup = 20;
    public int m_staticTroupPerRegrow = 1;
    [Header("Graphics and Meca Turn")]
    public float m_realtimeTurnInSeconds = 0.1f;
    [Header("Wallet")]
    // Time between people received money
    public float m_walletTurnInSeconds = 60f;
    // What amount of money player receive by turn
    public int m_walletTroupPerTurn = 50;
    // What amount of money player receive by turn
    [Header("Mining")]
    public int m_minerMiniumTroupPerTurn = 200;
    public double m_ghzMiningMinValue = 0.1;
    public int m_minerMaxiumTroupPerTurn = 1000;
    public double m_ghzMiningMaxValue = 22;

    [Header("Strat")]
    // HOw well defense troup has adventrage against attacking troup
    public float m_defenseMovingTroupAdventage=1.2f;
    // HOw well defense static troup has adventrage against attacking troup
    public float m_defenseStaticTroupAdventage=2.0f;


    [Header("Nuke")]
    public float m_nukeTzarCost = 100000;
    public float m_pourcentRadiusTzar = 0.1f;
    public float m_nukeAtomic = 10000;
    public float m_pourcentRadiusAtomic = 0.05f;
    public float m_nukeMini = 1000;
    public float m_pourcentRadiusMini = 0.01f;


    [Header("Graphic")]
    public int m_overpopulatedPixelMovingTroupCount = 256;
    public int m_overpopulatedPixelStaticTroupCount = 200;

}
