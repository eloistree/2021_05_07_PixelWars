﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

     using UnityEngine;
 using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PourcentClickTexture : MonoBehaviour
{



    public RectTransform m_target;
    public Camera m_camera;
    public Vector2 m_localMousePosition;
    public Vector2 m_localPourcentPosition;
    public bool m_overRectransform;

 

    void Update()
    {

         m_localMousePosition = m_target.InverseTransformPoint(Input.mousePosition);
        m_localPourcentPosition.x =0.5f + (m_localMousePosition.x / (float) m_target.rect.width);
        m_localPourcentPosition.y = 0.5f + (m_localMousePosition.y / (float)m_target.rect.height);
        m_overRectransform = (m_target.rect.Contains(m_localMousePosition));
        
   

    }


  
}

