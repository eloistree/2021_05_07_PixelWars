﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct Airdrop
{
    public TeamColor m_colorId;
    public int m_troup;
    public int m_l2rPixel;
    public int m_b2tPixel;
    public int m_pixelRadius;
}
