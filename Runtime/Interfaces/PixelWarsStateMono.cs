﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelWarsStateMono : MonoBehaviour
{

    public Texture2D m_mapValidityUsed;
    public PixelWarsState m_mapState;

    void Awake()
    {
        m_mapState = new PixelWarsState(GetMapValidityArray(m_mapValidityUsed),(uint) m_mapValidityUsed.width, (uint) m_mapValidityUsed.height); 
    }

    private bool[] GetMapValidityArray(Texture2D mapValidityUsed)
    {
        Color[] color = mapValidityUsed.GetPixels();
        bool[] validity = new bool[color.Length];

        for (int i = 0; i < color.Length; i++)
        {
            validity[i] = (color[i].r > 0.8f && color[i].g > 0.8f && color[i].b > 0.8f && color[i].a > 0.8f );
        }
        return validity;
    }
    public bool IsValidePx(int left2RightPixel, int bot2TopPixel)
    {
        return m_mapState.IsValidePx(left2RightPixel, bot2TopPixel);
    }
    public bool IsValide(float left2RightPourcent, float bot2TopPourcent)
    {
        return m_mapState.IsValide(left2RightPourcent, bot2TopPourcent);
    }

    public void SetToZero(float left2RightPourcent, float bot2TopPourcent)
    {
        m_mapState.SetToZeroPixel(left2RightPourcent, bot2TopPourcent);
    }
    public void SetToZeroXY(int left2RightPixel, int top2BotPixel)
    {
        m_mapState.SetToZeroPixelXY(left2RightPixel, top2BotPixel);
    }

    public uint GetValidePixelCount()
    {
        return m_mapState.m_totalValidePixel;
    }

    public void AddDefense(int staticTroupCount, int teamId, uint l2r, uint b2t)
    {
        m_mapState.AddDefense(staticTroupCount, teamId, l2r, b2t);
    }
}

public class PixelWarsState : IPixelWarsState
{
    public uint m_totalSize;
    public uint m_totalValidePixel;

    internal void SetValidePixel(int[] intArray)
    {
        m_validePixelRegisterAsInteger = intArray;
        m_validePixelRegister = new bool[intArray.Length];
        for (int i = 0; i < intArray.Length; i++)
        {
            m_validePixelRegister[i] = m_validePixelRegisterAsInteger[i] == 1;
        }
    }

    public uint m_width;
    public uint m_height;
    [HideInInspector]
    public int[] m_claimedRegister;
    [HideInInspector]
    public int[] m_movingTroupRegister;
    [HideInInspector]
    public int[] m_staticTroupRegister;
    [HideInInspector]
    public int[] m_civilTroupRegister;
    [HideInInspector]
    public bool[] m_validePixelRegister;
    [HideInInspector]
    public int[] m_validePixelRegisterAsInteger;

    public PixelWarsState(uint width, uint height)
    {
        bool[] b = new bool[width * height];
        SetDimention(width, height);
        SetValidePixel(b);
        CountValidePixel();
    }
    public PixelWarsState(bool[] mapValidity, uint width, uint height)
    {

        SetDimention(width, height);
        SetValidePixel(mapValidity);
        CountValidePixel();
    }

    internal void SetClaimed(int[] intArray)
    {
        m_claimedRegister = intArray;
    }

    private void CountValidePixel()
    {
        m_totalValidePixel = 0;
        for (int i = 0; i < m_validePixelRegister.Length; i++)
        {
            if (m_validePixelRegister[i])
                m_totalValidePixel++;
        }
    }

    internal void SetMovingTroup(int[] intArray)
    {
        m_movingTroupRegister = intArray;
    }

    public void SetDimention(uint width, uint height) {
        m_height = height;
        m_width = width;
        m_totalSize = width * height;

        m_claimedRegister = new int[m_totalSize];
        m_movingTroupRegister = new int[m_totalSize];
        m_staticTroupRegister = new int[m_totalSize];
        m_civilTroupRegister = new int[m_totalSize];
        m_validePixelRegister = new bool [m_totalSize];
        m_validePixelRegisterAsInteger = new int[m_totalSize];
    }

    internal void SetStaticMove(int[] intArray)
    {
        m_staticTroupRegister = intArray;
    }

    public void SetValidePixel(bool [] validePixels) {
        if (m_validePixelRegister.Length != m_totalSize) {
            throw new System.Exception("Set the valide pixels should match the dimention choosed");
        }
        for (int i = 0; i < validePixels.Length; i++)
        {
            m_validePixelRegister[i] = validePixels[i];
            m_validePixelRegisterAsInteger[i] =( validePixels[i]?1:0);
        }
    }

    internal void SetCivilTroup(int[] intArray)
    {
        m_civilTroupRegister = intArray;
    }

    public void Nuke(Vector2 m_localPourcentPosition, int pixel=5)
    {
        GetPixel(m_localPourcentPosition.x, m_localPourcentPosition.y, out int px, out int py);
        for (int i = -pixel; i < pixel; i++)
        {
            for (int j = -pixel; j < pixel; j++)
            {
                GetIndexInBotTopPixel(px+i, py+j, out int index);
                SetToZero(index);
                
            }

        }
    }

    private void GetIndexInBotTopPixel(int px, int py, out int index)
    {
        index = px + py * (int)m_width;
    }

    public void LaserAttack(Vector2 m_localPourcentPosition)
    {
        GetPixel(m_localPourcentPosition.x, m_localPourcentPosition.y, out int index);
        SetToZero(index);

    }

    private void SetToZero(int index)
    {
        m_movingTroupRegister[index] = m_staticTroupRegister[index] = 0;
        m_claimedRegister[index] = 0;
    }

    public void GetPixel(float l2r, float b2t, out int l2rPixel, out int b2tPixel)
    {
        l2rPixel = (int)(m_width * l2r);
        b2tPixel = (int)(m_height * b2t);

    }
    public void GetPixel(float l2r, float b2t, out int index)
    {
        GetPixel(l2r, 1f-b2t, out int l2rp, out int b2tp);
        index =  l2rp + (b2tp * (int) m_width);

    }





    public int[] GetColorClaimed()
    {
        return m_claimedRegister;
    }

    public int[] GetMovingTroup()
    {
        return m_movingTroupRegister;
    }

    public uint GetPixelCount()
    {
        return m_totalSize;
    }

    public uint GetPixelHeight()
    {
        return m_height;
    }

    public uint GetPixelWidth()
    {
        return m_width;
    }

   

    public int[] GetStaticTroup()
    {
        return m_staticTroupRegister;
    }

    public bool[] GetValidePixel()
    {
        return m_validePixelRegister;
    }


    public int[] GetValidePixelAsInterger()
    {
        return m_validePixelRegisterAsInteger;
    }





    #region A
    public void AddTroup(Vector2 pourcentBotLeftToTopRight, int soldiers, TroupType troupType , Color color)
    {
        AddTroup(pourcentBotLeftToTopRight.x, pourcentBotLeftToTopRight.y, soldiers, troupType, color);
    }
    public void AddTroup(int index1D, int soldiers, TroupType troupType, Color color)
    {

        int colorId = (int)((color.r * 255f) * 1000000f + (color.g * 255f) * 1000f + (color.b * 255f));
        if (troupType == TroupType.Moving)
        {
            m_movingTroupRegister[index1D] += soldiers;
        }
        else { 
            m_staticTroupRegister[index1D] += soldiers;
        }      
        m_claimedRegister[index1D] = colorId;

    }
    public void AddTroup(float left2Right, float bot2top, int soldiers, TroupType troupType, Color color)
    {

        GetPixelXY(left2Right, bot2top, out int index);
        AddTroup(index, soldiers, troupType, color);
    }

    public int GetIndex(int x, int y)
    {
        return (y * (int)m_width) + (x);
    }
    public void GetPixelXY(float left2Right, float top2Down, out int index)
    {
        GetPixelXY(left2Right, top2Down, out int x, out int y);

        index = (y * (int) m_width) + (x);
    }
    public void GetPixelXY(float left2Right, float top2Down, out int left2RightPixel, out int bot2TopPixel)
    {

        left2RightPixel = (int)(left2Right * (float)m_width);
        bot2TopPixel = (int)(top2Down * (float)m_height);
    }

    public bool IsValide(float left2RightPourcent, float bot2TopPourcent)
    {
        GetPixelXY(left2RightPourcent, bot2TopPourcent, out int index);
        //GetPixelXY(left2RightPourcent, 1f - bot2TopPourcent, out int index);
        if (index < 0 || index >= m_totalSize)
            return false;
        return m_validePixelRegister[index];
    }
    public bool IsValidePx(int x, int y)
    {
       int index = GetIndex(x, y);
        if (index < 0 || index >= m_totalSize)
            return false;
        return m_validePixelRegister[index];
    }

    public void SetToZeroPixel(float left2RightPourcent, float bot2TopPourcent)
    {
        GetPixelXY(left2RightPourcent, 1f - bot2TopPourcent, out int index);

        m_movingTroupRegister[index] = 0;
        m_staticTroupRegister[index] = 0;
        m_claimedRegister[index] = 0;

    }
    public void GetPixelXY(int x, int y, out int index)
    {
        index = (y * (int)m_width) + (x);
    }
    public void SetToZeroPixelXY(int x, int y)
    {
        GetPixelXY(x, y, out int index);

        m_movingTroupRegister[index] = 0;
        m_staticTroupRegister[index] = 0;
        m_claimedRegister[index] = 0;

    }

    public void AddDefense(int staticTroupCount, int teamColor, uint l2r, uint b2t)
    {
        if (teamColor < 1)
        {
            return;
        }
        if (staticTroupCount<1)
        {
            return;
        }

        int i = GetIndex((int)l2r, (int)b2t);
        if (m_validePixelRegister[i])
        {
            if (m_claimedRegister[i] == 0)
            {
                m_claimedRegister[i] = teamColor;

            }
            if (m_claimedRegister[i] == teamColor)
            {
                m_staticTroupRegister[i] += staticTroupCount;

            }
            
        }
    }
    #endregion

























}
