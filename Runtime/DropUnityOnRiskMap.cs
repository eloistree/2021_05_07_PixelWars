﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropUnityOnRiskMap : MonoBehaviour
{

    public QuickTestPixelRISK m_risk;

    public PixelWarsStateMono m_pixelWars;

    public PourcentClickTexture m_isOnTexture;

    public int m_unityDropOnLeftClick=10;
    public int m_unityDropOnMaintaintClick =1;


    void Update()
    {

        if (m_risk)
        {
            if (Input.GetMouseButton(0) && m_isOnTexture.m_overRectransform)
                m_risk.AddUnity(m_isOnTexture.m_localPourcentPosition, m_unityDropOnLeftClick, Color.blue);

            if (Input.GetMouseButton(1) && m_isOnTexture.m_overRectransform)
                m_risk.AddUnity(m_isOnTexture.m_localPourcentPosition, m_unityDropOnLeftClick, Color.blue);
          
            if (Input.GetMouseButton(2) && m_isOnTexture.m_overRectransform)
                m_risk.AddUnity(m_isOnTexture.m_localPourcentPosition, m_unityDropOnLeftClick, Color.green);


            if (Input.GetKey(KeyCode.E) && m_isOnTexture.m_overRectransform)
                m_risk.AddUnity(m_isOnTexture.m_localPourcentPosition, m_unityDropOnMaintaintClick, Color.yellow);

            if (Input.GetKey(KeyCode.R) && m_isOnTexture.m_overRectransform)
                m_risk.AddUnity(m_isOnTexture.m_localPourcentPosition, m_unityDropOnMaintaintClick , Color.yellow);

        }
        if (m_pixelWars && m_isOnTexture.m_overRectransform)
        {
            if (Input.GetMouseButton(0) && m_isOnTexture.m_overRectransform)
                m_pixelWars.m_mapState.AddTroup(m_isOnTexture.m_localPourcentPosition, m_unityDropOnLeftClick, TroupType.Moving, Color.blue);

            if (Input.GetMouseButton(1) && m_isOnTexture.m_overRectransform)
                m_pixelWars.m_mapState.AddTroup(m_isOnTexture.m_localPourcentPosition, m_unityDropOnLeftClick, TroupType.Static, Color.blue);
            if (Input.GetMouseButton(2) && m_isOnTexture.m_overRectransform)
                m_pixelWars.m_mapState.AddTroup(m_isOnTexture.m_localPourcentPosition, m_unityDropOnLeftClick, TroupType.Moving, Color.green);


            if (Input.GetKey(KeyCode.E) && m_isOnTexture.m_overRectransform)
                m_pixelWars.m_mapState.AddTroup(m_isOnTexture.m_localPourcentPosition, m_unityDropOnMaintaintClick, TroupType.Moving, Color.yellow);

            if (Input.GetKey(KeyCode.R) && m_isOnTexture.m_overRectransform)
                m_pixelWars.m_mapState.AddTroup(m_isOnTexture.m_localPourcentPosition, m_unityDropOnMaintaintClick, TroupType.Static, Color.yellow);

            if (Input.GetKeyDown(KeyCode.Y))
                m_pixelWars.m_mapState.Nuke(m_isOnTexture.m_localPourcentPosition, 20);

            if (Input.GetKeyDown(KeyCode.U))
                m_pixelWars.m_mapState.Nuke(m_isOnTexture.m_localPourcentPosition, 15);

            if (Input.GetKeyDown(KeyCode.I))
                m_pixelWars.m_mapState.Nuke(m_isOnTexture.m_localPourcentPosition, 10);

            if (Input.GetKeyDown(KeyCode.O))
                m_pixelWars.m_mapState.Nuke(m_isOnTexture.m_localPourcentPosition, 5);

            if (Input.GetKey(KeyCode.L) )
                m_pixelWars.m_mapState.LaserAttack(m_isOnTexture.m_localPourcentPosition);




        }



    }
}
