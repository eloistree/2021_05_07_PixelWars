﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// When a player do an action, I should verify is the player is authorized or if the request is valide.
/// In this class I am not doing it because of rushing code. But in clean version I should do it in way.
public  abstract class PixelWarsPlayerRequestInterface : MonoBehaviour
{




    public abstract void RequestAirDrop(UnityPlayerCursorInformation playerCursor, AirDropOnceBean airdrop, TroupWallet wallet);
    public abstract void RequestAirDropFullFlush(UnityPlayerCursorInformation playerCursor, TeamColor teamOwner, TroupWallet wallet);

    public abstract void SetContinusAutoAirDrop(UnityPlayerCursorInformation playerCursor, AutoAirDropBean autoAirdrop, TroupWallet wallet);

    public abstract void RequestNuke(UnityPlayerCursorInformation playerCursor, NukeType actionType, TroupWallet wallet);
    public abstract void RequestLaserStrike(UnityPlayerCursorInformation playerCursor, TroupWallet wallet);
    public abstract void RequestVirus(UnityPlayerCursorInformation playerCursor, VirusType actionType, TroupWallet wallet);
    public abstract void RequestDoomStrike(UnityPlayerCursorInformation playerCursor, FUckThisAreaType actionType, TroupWallet wallet);

    public abstract void RequestLaserStrike(PlayerNameId id, UnityPlayerCursorInformation cursor, TroupWallet wallet);

    public abstract void RequestLaserStrike(PlayerNameId id, PourcentToPixelCoordinate where, TroupWallet wallet);

    public abstract void RequestDefense(PlayerNameId id, UnityPlayerCursorInformation cursor, TroupWallet wallet, uint troupToSend, int radiusInPixel);

}

public enum NukeType { 
    H, Atomic, Mini, Airstrike 
}
public enum VirusType { 
    Covid, BlackPlague, WarPlague 
}
public enum AIAttackType {
    Terminator, // Only kill troup
    Saboteur, // Only defense troup
    SmartMine // Kill less but kill troup or defense
}

public enum FUckThisAreaType{
    AirNanobot, //  Kill randomly troup in the zone
    RadiationSpreading // kill periodicaly troup in the zone
}




[System.Serializable]
public class TroupWallet {
    decimal m_troupInWallet = new decimal();

    public decimal GetCurrentWallet() { return m_troupInWallet; }
    public void Remove(uint troup) { m_troupInWallet -= troup; }
    public void Add(uint troup) { m_troupInWallet += troup; }
    public void SetTo(uint troup) { m_troupInWallet = troup; }
    public void SetTo(decimal troup) { m_troupInWallet = troup; }
    public uint Claim(uint troup)
    {
        if (m_troupInWallet <= 0)
        {
            return 0;
        }
        if (m_troupInWallet > troup)
        {
            Remove(troup);
            return troup;
        }
        uint lastTroup = troup;
        m_troupInWallet = 0;
        return lastTroup;
        
    }

    public void ClaimAll(out uint claimed)
    {
        claimed = Claim(uint.MaxValue);
    }
    public void ClaimAll(out int claimed)
    {
        claimed = (int)Claim(int.MaxValue);
    }
    public void ClaimAll(out decimal claimed)
    {
        claimed = m_troupInWallet;
        m_troupInWallet = new decimal(0);
    }
}


public class AirDropOnceBean 
{
    public uint m_troup;
    public TeamColor m_teamOfTheDrop;

    public AirDropOnceBean(uint troup, TeamColor team)
    {
        m_teamOfTheDrop = team;
        m_troup = troup;
    }

    public void Set(uint troup) { m_troup = troup; }
}


public class AutoAirDropBean
{
    public bool m_isActive;
    public uint m_troupToDropPerSecondPast;
    public uint m_troupInqueueToBeDeplay;
    public TeamColor m_teamOfTheDrop;

    public AutoAirDropBean(bool isActive, uint troupToDropPerSecondPast, uint troupInqueueToBeDeplay, TeamColor team)
    {
        m_isActive = isActive;
        m_teamOfTheDrop = team;
        m_troupToDropPerSecondPast = troupToDropPerSecondPast;
        m_troupInqueueToBeDeplay = troupInqueueToBeDeplay;
    }

  
    public void SetOn(bool setIsOn) { m_isActive = setIsOn; }
    public void SetFlux(uint troupToDropPerSecondPast ) { m_troupToDropPerSecondPast = troupToDropPerSecondPast; }

    public void AddInQueue(uint troups)
    {
        m_troupInqueueToBeDeplay += troups;
    }


}
