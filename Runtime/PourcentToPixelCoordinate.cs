﻿
using System.Collections.Generic;
using UnityEngine;

public class PourcentToPixelCoordinate
{

    public float m_l2rPourcent;
    public float m_b2tPourcent;

    public PourcentToPixelCoordinate(float l2rPourcent, float b2tPourcent)
    {
        m_l2rPourcent = l2rPourcent;
        m_b2tPourcent = b2tPourcent;
    }
    public PourcentToPixelCoordinate()
    {
        m_l2rPourcent = 0;
        m_b2tPourcent = 0;
    }

    public float GetLeft2Right() { return m_l2rPourcent; }
    public float GetBot2Top() { return m_b2tPourcent; }
    public float GetTop2Bot() { return 1f - m_b2tPourcent; }

    public void GetLeft2RightPx(int width, out uint l2rPx) { l2rPx = (uint)(m_l2rPourcent * width); }
    public void GetBot2TopPx(int height, out uint b2tPx) { b2tPx = (uint)(m_b2tPourcent * height); }
    public void GetTop2BotPx(int height, out uint t2bPx) { t2bPx = (uint)((1f - m_b2tPourcent) * height); }

    public void GetLeft2RightPx(int width, out int l2rPx) { l2rPx = (int)(m_l2rPourcent * width); }
    public void GetBot2TopPx(int height, out int b2tPx) { b2tPx = (int)(m_b2tPourcent * height); }
    public void GetTop2BotPx(int height, out int t2bPx) { t2bPx = (int)((1f - m_b2tPourcent) * height); }



    public void ApplySquareOnPixels(ref uint width, ref uint height, ref int radius, CpuActionsOnPixel apply)
    {
        if (apply == null)
            return;

        GetLeft2RightPx((int)width, out int cx);
        //  GetTop2BotPx((int)height, out int cy); 
        GetBot2TopPx((int)height, out int cy);

        for (int x = -radius; x < radius; x++)
        {
            for (int y = -radius; y < radius; y++)
            {
                int dx = cx + x;
                int dy = cy + y;
                if (dx >= 0 && dx < width && dy >= 0 && dy < height)
                {

                    apply(dx, dy);
                }
            }
        }
    }

    public void GetPixelsInCircleAround(ref uint width, ref uint height, ref int radius, out List<PixelL2RB2T> pixels)
    {
        pixels = new List<PixelL2RB2T>();

        GetLeft2RightPx((int)width, out int cx);
        GetBot2TopPx((int)height, out int cy);

        for (int x = -radius; x < radius; x++)
        {
            for (int y = -radius; y < radius; y++)
            {
                int dx = cx + x;
                int dy = cy + y;
                if (dx >= 0 && dx < width && dy >= 0 && dy < height)
                {
                    if (Mathf.Abs(Mathf.Sqrt(x * x + y * y)) <= (float)radius)
                        pixels.Add(new PixelL2RB2T((uint)dx, (uint)dy));
                }
            }
        }
    }
    public void ApplyCircleOnPixels(ref uint width, ref uint height, ref int radius, CpuActionsOnPixel apply)
    {
        if (apply == null)
            return;

        GetLeft2RightPx((int)width, out int cx);
        //  GetTop2BotPx((int)height, out int cy); 
        GetBot2TopPx((int)height, out int cy);

        for (int x = -radius; x < radius; x++)
        {
            for (int y = -radius; y < radius; y++)
            {
                int dx = cx + x;
                int dy = cy + y;
                if (dx >= 0 && dx < width && dy >= 0 && dy < height)
                {
                    if (Mathf.Abs(Mathf.Sqrt(x * x + y * y)) <= (float)radius)
                        apply(dx, dy);
                }
            }
        }
    }
    public delegate void CpuActionsOnPixel(int x, int y);

    public void GetAsPixel(ref uint width, ref uint height, out PixelL2RB2T result)
    {
        GetLeft2RightPx((int)width, out int cx);
        //  GetTop2BotPx((int)height, out int cy); 
        GetBot2TopPx((int)height, out int cy);
        result = new PixelL2RB2T((uint)cx, (uint)cy);
    }
}
public struct PixelL2RB2T
{
    public uint m_l2r;
    public uint m_b2t;

    public PixelL2RB2T(uint l2r, uint b2t)
    {
        m_l2r = l2r;
        m_b2t = b2t;
    }

}