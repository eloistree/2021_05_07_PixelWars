﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PixelWarsTurns : MonoBehaviour
{
    public float m_turnsInSeconds=0.1f;

    public UnityEvent m_turnPing;

    public List<QuickTimeLoop> m_timers = new List<QuickTimeLoop>();

    public IEnumerator Start() {

        while (true) {

            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_turnsInSeconds);
            m_turnPing.Invoke();
        }

    }


    private void Update()
    {
        for (int i = 0; i < m_timers.Count; i++)
        {
            m_timers[i].Remove(Time.deltaTime);
        }
    }
}


[System.Serializable]
public class QuickTimeLoop {

    public string m_name;
    public Text m_debugTextName;
    public float m_timeLoop;
    public float m_timeLeft;
    public Text m_debugTextTimeLeft;
    public UnityEvent m_actions;

    public void Remove(float deltaTime)
    {
        m_timeLeft -= deltaTime;
        if (m_timeLeft < 0) {
            m_timeLeft = m_timeLoop;
            
            m_actions.Invoke();
        }
        if(m_debugTextName != null)
            m_debugTextName.text = m_name;
        if (m_debugTextTimeLeft != null)
            m_debugTextTimeLeft.text = string.Format("{0:0}", m_timeLeft);
    }
}